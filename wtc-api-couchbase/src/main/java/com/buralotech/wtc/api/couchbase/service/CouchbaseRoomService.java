/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.couchbase.service;

import com.buralotech.wtc.api.couchbase.domain.CouchbaseRoom;
import com.buralotech.wtc.api.couchbase.repository.CouchbaseRoomRepository;
import com.buralotech.wtc.api.domain.Room;
import com.buralotech.wtc.api.service.database.AbstractRoomService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CouchbaseRoomService extends AbstractRoomService {

    private final CouchbaseRoomRepository roomRepository;

    CouchbaseRoomService(final IdentifierService identifierService,
                         final CouchbaseRoomRepository roomRepository) {
        super(identifierService);
        this.roomRepository = roomRepository;
    }

    @Override
    public Flux<Room> fetchRooms() {
        return roomRepository
                .findAll(Sort.by("name"))
                .cast(Room.class);
    }

    @Override
    public Mono<Room> createRoom(final String name,
                                 final boolean secret) {
        return roomRepository
                .findByName(name)
                .cast(Room.class)
                .switchIfEmpty(doCreateRoom(name, secret));
    }

    private Mono<Room> doCreateRoom(final String name,
                                    final boolean secret) {
        return getIdentifierService()
                .generateIdentifier()
                .map(id -> new CouchbaseRoom(id, name, secret))
                .flatMap(roomRepository::save)
                .cast(Room.class);
    }

    @Override
    public Mono<Room> getRoom(final String id) {
        return roomRepository
                .findById(id)
                .cast(Room.class);
    }

    @Override
    public Mono<Void> updateRoom(final String id,
                                 final String name,
                                 final boolean secret) {
        return roomRepository
                .findById(id)
                .map(room -> new CouchbaseRoom(id, name, secret))
                .flatMap(roomRepository::save)
                .then();
    }

    @Override
    public Mono<Void> deleteRoom(final String id) {
        return roomRepository.deleteById(id);
    }
}
