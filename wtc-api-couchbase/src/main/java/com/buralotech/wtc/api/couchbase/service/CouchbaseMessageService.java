/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.couchbase.service;

import com.buralotech.wtc.api.couchbase.domain.CouchbaseMessage;
import com.buralotech.wtc.api.couchbase.repository.CouchbaseMessageRepository;
import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.service.database.AbstractMessageService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Clock;

@Service
public class CouchbaseMessageService extends AbstractMessageService {

    private final CouchbaseMessageRepository messageRepository;

    CouchbaseMessageService(final IdentifierService identifierService,
                            final Clock clock,
                            final CouchbaseMessageRepository messageRepository) {
        super(identifierService, clock);
        this.messageRepository = messageRepository;
    }

    @Override
    public Mono<Void> createMessageStream(final String room) {
        return Mono.empty();
    }

    @Override
    public Mono<Void> deleteMessageStream(String room) {
        return Mono.empty();
    }

    @Override
    public Flux<Message> fetchMessages(final String room,
                                       final String marker) {
        return messageRepository
                .findMessagesByRoomAndMessageTimestampGreaterThanOrderByMessageTimestamp(room, Long.parseLong(marker))
                .cast(Message.class);
    }

    @Override
    public Flux<Message> streamMessages(final String room,
                                        final String marker) {
        return fetchMessages(room, marker);
    }

    @Override
    public Mono<Message> postMessage(final String room,
                                     final String sender,
                                     final String language,
                                     final String text) {
        return getIdentifierService()
                .generateIdentifier()
                .map(id -> new CouchbaseMessage(id, sender, language, text, getClock().millis()))
                .flatMap(messageRepository::save)
                .cast(Message.class);
    }
}
