package com.buralotech.wtc.api.couchbase.repository;

import com.buralotech.wtc.api.couchbase.domain.CouchbaseRoom;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository;
import reactor.core.publisher.Mono;

@N1qlPrimaryIndexed
public interface CouchbaseRoomRepository extends ReactiveCouchbaseRepository<CouchbaseRoom, String> {

    Mono<CouchbaseRoom> findByName(String name);
}
