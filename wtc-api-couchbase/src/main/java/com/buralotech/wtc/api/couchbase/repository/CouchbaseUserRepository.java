package com.buralotech.wtc.api.couchbase.repository;

import com.buralotech.wtc.api.couchbase.domain.CouchbaseUser;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository;
import reactor.core.publisher.Mono;

@N1qlPrimaryIndexed
public interface CouchbaseUserRepository extends ReactiveCouchbaseRepository<CouchbaseUser, String> {

    Mono<CouchbaseUser> findByNicknameOrEmailAddress(String nickname, String emailAddress);
}
