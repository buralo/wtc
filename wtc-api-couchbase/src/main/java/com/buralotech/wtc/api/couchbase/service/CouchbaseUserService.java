/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.couchbase.service;

import com.buralotech.wtc.api.couchbase.domain.CouchbaseUser;
import com.buralotech.wtc.api.couchbase.repository.CouchbaseUserRepository;
import com.buralotech.wtc.api.domain.User;
import com.buralotech.wtc.api.domain.UserFactory;
import com.buralotech.wtc.api.service.database.AbstractUserService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.data.domain.Sort;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
public class CouchbaseUserService extends AbstractUserService {

    private final CouchbaseUserRepository userRepository;

    CouchbaseUserService(final UserFactory userFactory,
                         final IdentifierService identifierService,
                         final JavaMailSender emailSender,
                         final CouchbaseUserRepository userRepository) {
        super(userFactory, identifierService, emailSender);
        this.userRepository = userRepository;
    }

    @Override
    public Flux<User> fetchUsers() {
        return userRepository
                .findAll(Sort.by("name"))
                .cast(User.class);
    }

    @Override
    public Mono<User> createUser(final String name,
                                 final String nickname,
                                 final String emailAddress) {
        return userRepository.findByNicknameOrEmailAddress(nickname, emailAddress)
                .cast(User.class)
                .switchIfEmpty(
                        createUserEntity(name, nickname, emailAddress)
                                .cast(CouchbaseUser.class)
                                .flatMap(userRepository::save)
                                .cast(User.class));
    }

    @Override
    public Mono<User> getUser(final String id) {
        return userRepository
                .findById(id)
                .cast(User.class);
    }

    @Override
    public Mono<Void> updateUser(final String id,
                                 final String name,
                                 final String nickname,
                                 final String emailAddress) {
        return userRepository
                .findById(id)
                .map(user -> new CouchbaseUser(id, name, nickname, emailAddress, user.getToken(), user.getPassword()))
                .flatMap(userRepository::save)
                .then();
    }

    @Override
    public Mono<Void> setUserPassword(final String id,
                                      final String token,
                                      final String newPassword) {
        return userRepository
                .findById(id)
                .filter(user -> Objects.equals(token, user.getToken()))
                .map(user -> new CouchbaseUser(id, user.getName(), user.getNickname(), user.getEmailAddress(), null, newPassword))
                .flatMap(userRepository::save)
                .then();
    }

    @Override
    public Mono<Void> changeUserPassword(final String id,
                                         final String oldPassword,
                                         final String newPassword) {
        return userRepository
                .findById(id)
                .filter(user -> oldPassword.equals(user.getPassword()))
                .map(user -> new CouchbaseUser(id, user.getName(), user.getNickname(), user.getEmailAddress(), null, newPassword))
                .flatMap(userRepository::save)
                .then();
    }

    @Override
    public Mono<Void> deleteUser(final String id) {
        return userRepository
                .deleteById(id);
    }
}
