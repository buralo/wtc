package com.buralotech.wtc.api.couchbase.domain;

import com.buralotech.wtc.api.domain.User;
import org.springframework.data.annotation.Id;

public class CouchbaseUser implements User {

    @Id
    private final String id;

    private final String name;

    private final String nickname;

    private final String emailAddress;

    private final String token;

    private final String password;

    public CouchbaseUser(final String id,
                         final String name,
                         final String nickname,
                         final String emailAddress,
                         final String token,
                         final String password) {
        this.id = id;
        this.name = name;
        this.nickname = nickname;
        this.emailAddress = emailAddress;
        this.token = token;
        this.password = password;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getNickname() {
        return nickname;
    }

    @Override
    public String getEmailAddress() {
        return emailAddress;
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public String getPassword() {
        return password;
    }
}
