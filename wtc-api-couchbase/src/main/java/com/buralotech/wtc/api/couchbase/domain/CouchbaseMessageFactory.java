package com.buralotech.wtc.api.couchbase.domain;

import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.domain.MessageFactory;
import org.springframework.stereotype.Component;

@Component
public class CouchbaseMessageFactory implements MessageFactory {
    @Override
    public Message createMessage(final String id,
                                 final String sender,
                                 final String messageLanguage,
                                 final String messageText,
                                 final long messageTimestamp) {
        return new CouchbaseMessage(
                id,
                sender,
                messageLanguage,
                messageText,
                messageTimestamp);
    }
}
