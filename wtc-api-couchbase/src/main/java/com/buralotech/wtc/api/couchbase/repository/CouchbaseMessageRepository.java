package com.buralotech.wtc.api.couchbase.repository;

import com.buralotech.wtc.api.couchbase.domain.CouchbaseMessage;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository;
import reactor.core.publisher.Flux;

@N1qlPrimaryIndexed
public interface CouchbaseMessageRepository extends ReactiveCouchbaseRepository<CouchbaseMessage, String> {

    Flux<CouchbaseMessage> findMessagesByRoomAndMessageTimestampGreaterThanOrderByMessageTimestamp(String room, long messageTimestamp);
}
