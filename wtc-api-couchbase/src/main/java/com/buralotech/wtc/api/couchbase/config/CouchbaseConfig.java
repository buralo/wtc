/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.couchbase.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractReactiveCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableReactiveCouchbaseRepositories;

import java.util.List;

@Configuration
@EnableConfigurationProperties(CouchbaseProperties.class)
@EnableReactiveCouchbaseRepositories("com.buralotech.wtc.api.couchbase.repository")
public class CouchbaseConfig extends AbstractReactiveCouchbaseConfiguration {

    @Autowired
    private CouchbaseProperties properties;

    @Override
    protected List<String> getBootstrapHosts() {
        return properties.getBootstrapHosts();
    }

    @Override
    protected String getBucketName() {
        return properties.getBucketName();
    }

    @Override
    protected String getBucketPassword() {
        return properties.getBucketPassword();
    }
}
