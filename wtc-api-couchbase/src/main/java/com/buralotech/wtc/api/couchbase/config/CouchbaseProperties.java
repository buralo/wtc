package com.buralotech.wtc.api.couchbase.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties("couchbase")
public class CouchbaseProperties {

    private final List<String> bootstrapHosts = new ArrayList<>();

    private String bucketName;

    private String bucketPassword;

    public List<String> getBootstrapHosts() {
        return bootstrapHosts;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(final String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketPassword() {
        return bucketPassword;
    }

    public void setBucketPassword(final String bucketPassword) {
        this.bucketPassword = bucketPassword;
    }
}
