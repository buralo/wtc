package com.buralotech.wtc.api.couchbase.domain;

import com.buralotech.wtc.api.domain.Room;
import org.springframework.data.annotation.Id;

public class CouchbaseRoom implements Room {

    @Id
    private final String id;

    private final String name;

    private final boolean secret;

    public CouchbaseRoom(final String id,
                         final String name,
                         final boolean secret) {
        this.id = id;
        this.name = name;
        this.secret = secret;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isSecret() {
        return secret;
    }
}
