package com.buralotech.wtc.api.couchbase.domain;

import com.buralotech.wtc.api.domain.User;
import com.buralotech.wtc.api.domain.UserFactory;
import org.springframework.stereotype.Component;

@Component
public class CouchbaseUserFactory implements UserFactory {
    @Override
    public User createUser(final String id,
                           final String name,
                           final String nickname,
                           final String emailAddress,
                           final String token,
                           final String password) {
        return new CouchbaseUser(id, name, nickname, emailAddress, token, password);
    }
}
