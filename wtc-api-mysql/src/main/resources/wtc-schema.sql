CREATE SCHEMA IF NOT EXISTS wtc;

CREATE TABLE IF NOT EXISTS wtc.messages (
    id VARCHAR(36) NOT NULL,
    room VARCHAR(36) NOT NULL,
    sender VARCHAR(36) NOT NULL,
    message_language VARCHAR(2) NOT NULL,
    message_text TEXT NOT NULL,
    message_timestamp BIGINT NOT NULL,
    PRIMARY KEY (id),
    INDEX (room)
);

CREATE TABLE IF NOT EXISTS wtc.rooms (
    id VARCHAR(36) NOT NULL,
    name VARCHAR(36) NOT NULL,
    secret char(1) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE INDEX (name)
);

CREATE TABLE IF NOT EXISTS wtc.occupants (
    room_id VARCHAR(36) NOT NULL,
    user_id VARCHAR(36) NOT NULL,
    role VARCHAR(12) NOT NULL,
    PRIMARY KEY (room_id, user_id, role)
);

CREATE TABLE IF NOT EXISTS wtc.users (
    id VARCHAR(36) NOT NULL,
    name VARCHAR(100) NOT NULL,
    nickname VARCHAR(20) NOT NULL,
    email_address VARCHAR(200) NOT NULL,
    password VARCHAR(100),
    token VARCHAR(1000),
    PRIMARY KEY (id),
    UNIQUE INDEX (nickname),
    UNIQUE INDEX (email_address)
);

INSERT INTO wtc.rooms (id, name, secret) VALUES ('ac02acf8-6d62-4f5b-9aec-7175451ab6fb', 'General', 'N');
INSERT INTO wtc.rooms (id, name, secret) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', 'Matthews Family', 'Y');
INSERT INTO wtc.users (id, name, nickname, email_address, password) VALUES ('912e86f0-2e38-4987-bc10-8bb1930173a6', 'Brian Matthews', 'bmatthews68', 'brian@btmatthews.com', 'password');
INSERT INTO wtc.users (id, name, nickname, email_address, password) VALUES ('7b40e339-1a3e-4c03-bed1-8ab1749d95f1', 'Elena Popova', 'elena77', 'elena@btmatthews.com', 'password');
INSERT INTO wtc.users (id, name, nickname, email_address, password) VALUES ('5071f75c-5e49-4ef4-be4b-e651534fc57c', 'Yaromir Popov Matthews', 'yaromir05', 'yaromir@btmatthews.com', 'password');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '912e86f0-2e38-4987-bc10-8bb1930173a6', 'OWNER');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '912e86f0-2e38-4987-bc10-8bb1930173a6', 'MODERATOR');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '912e86f0-2e38-4987-bc10-8bb1930173a6', 'CONTRIBUTOR');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '7b40e339-1a3e-4c03-bed1-8ab1749d95f1', 'MODERATOR');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '7b40e339-1a3e-4c03-bed1-8ab1749d95f1', 'CONTRIBUTOR');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '5071f75c-5e49-4ef4-be4b-e651534fc57c', 'CONTRIBUTOR');