package com.buralotech.wtc.api.mysql.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ByteToBooleanConverter implements Converter<Byte, Boolean> {

    @Override
    public Boolean convert(Byte source) {
        return source == 0 ? Boolean.FALSE : Boolean.TRUE;
    }
}
