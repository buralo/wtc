/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.redis.domain;

import com.buralotech.wtc.api.domain.Room;

public class RedisRoom implements Room {

    private String id;

    private String name;

    private boolean secret;

    public RedisRoom() {
    }

    public RedisRoom(final String id,
                     final String name,
                     final boolean secret) {
        this.id = id;
        this.name = name;
        this.secret = secret;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public boolean isSecret() {
        return secret;
    }

    public void setSecret(final boolean secret) {
        this.secret = secret;
    }
}
