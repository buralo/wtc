/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.redis.domain;

import com.buralotech.wtc.api.domain.Message;

public class RedisMessage implements Message {

    private String id;

    private String sender;

    private String messageLanguage;

    private String messageText;

    private long messageTimestamp;

    public RedisMessage() {
    }

    public RedisMessage(final String id,
                        final String sender,
                        final String messageLanguage,
                        final String messageText,
                        final long messageTimestamp) {
        this.id = id;
        this.sender = sender;
        this.messageLanguage = messageLanguage;
        this.messageText = messageText;
        this.messageTimestamp = messageTimestamp;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getSender() {
        return sender;
    }

    @Override
    public String getMessageLanguage() {
        return messageLanguage;
    }

    @Override
    public String getMessageText() {
        return messageText;
    }

    @Override
    public long getMessageTimestamp() {
        return messageTimestamp;
    }

    @Override
    public String getMarker() {
        return id;
    }
}
