/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.redis.service;

import com.buralotech.wtc.api.domain.Room;
import com.buralotech.wtc.api.redis.domain.RedisRoom;
import com.buralotech.wtc.api.service.database.AbstractRoomService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class RedisRoomService extends AbstractRoomService {

    private static final String ROOMS = "rooms";

    private final ReactiveRedisOperations<String, RedisRoom> roomOperations;

    RedisRoomService(final IdentifierService identifierService,
                     final ReactiveRedisOperations<String, RedisRoom> roomOperations) {
        super(identifierService);
        this.roomOperations = roomOperations;
    }

    @Override
    public Flux<Room> fetchRooms() {
        return roomOperations
                .opsForList()
                .range(ROOMS, 0L, Long.MAX_VALUE)
                .cast(Room.class);
    }

    @Override
    public Mono<Room> createRoom(final String name,
                                 final boolean secret) {
        return roomOperations
                .opsForList()
                .range(ROOMS, 0L, Long.MAX_VALUE)
                .filter(room -> name.equals(room.getName()))
                .next()
                .switchIfEmpty(Mono.defer(() -> doCreate(name, secret)))
                .cast(Room.class);
    }

    private Mono<RedisRoom> doCreate(final String name,
                                     final boolean secret) {
        return getIdentifierService()
                .generateIdentifier()
                .map(id -> new RedisRoom(id, name, secret))
                .flatMap(room ->
                        roomOperations
                                .opsForList()
                                .rightPush(ROOMS, room)
                                .thenReturn(room));
    }

    @Override
    public Mono<Room> getRoom(final String id) {
        return roomOperations
                .opsForList()
                .range(ROOMS, 0L, Long.MAX_VALUE)
                .filter(room -> id.equals(room.getId()))
                .next()
                .cast(Room.class);
    }

    @Override
    public Mono<Void> updateRoom(final String id,
                                 final String name,
                                 final boolean secret) {
        return roomOperations
                .opsForList()
                .range(ROOMS, 0L, Long.MAX_VALUE)
                .next()
                .zipWith(Mono.defer(() -> Mono.just(new RedisRoom(id, name, secret))))
                .flatMap(tuple ->
                        roomOperations
                                .opsForList()
                                .remove(ROOMS, 1, tuple.getT1())
                                .then(roomOperations.opsForList().rightPush(ROOMS, tuple.getT2()).then()));
    }

    @Override
    public Mono<Void> deleteRoom(final String id) {
        return roomOperations
                .opsForList()
                .range(ROOMS, 0L, Long.MAX_VALUE)
                .filter(room -> id.equals(room.getId()))
                .map(room -> roomOperations.opsForList().remove(ROOMS, 1, room))
                .then();
    }
}
