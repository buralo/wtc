 /*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.redis.service;

import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.redis.domain.RedisMessage;
import com.buralotech.wtc.api.service.database.AbstractMessageService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.data.domain.Range;
import org.springframework.data.redis.connection.RedisZSetCommands;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.Record;
import org.springframework.data.redis.connection.stream.RecordId;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Clock;

@Service
public class RedisMessageService extends AbstractMessageService {

    private static final String PREFIX = "messages:";

    private final ReactiveRedisOperations<String, String> messageOperations;

    RedisMessageService(final IdentifierService identifierService,
                        final Clock clock,
                        final ReactiveRedisOperations<String, String> messageOperations) {
        super(identifierService, clock);
        this.messageOperations = messageOperations;
    }

    @Override
    public Mono<Void> createMessageStream(String room) {
        return Mono.empty();
    }

    @Override
    public Mono<Void> deleteMessageStream(String room) {
        return Mono.empty();
    }

    @Override
    public Flux<Message> fetchMessages(final String room) {
        return doFetchMessages(
                room,
                Range.unbounded());
    }

    @Override
    public Flux<Message> fetchMessages(final String room,
                                       final String marker) {
        return doFetchMessages(
                room,
                Range.rightUnbounded(Range.Bound.exclusive(marker)));
    }

    private Flux<Message> doFetchMessages(final String room,
                                          final Range<String> range) {
        return messageOperations
                .<String, String>opsForStream()
                .range(
                        MessageItem.class,
                        toKey(room),
                        range,
                        RedisZSetCommands.Limit
                                .limit()
                                .offset(0)
                                .count(Integer.MAX_VALUE))
                .map(this::toMessage);
    }

    @Override
    public Flux<Message> streamMessages(final String room) {
        return fetchMessages(room);
    }

    @Override
    public Flux<Message> streamMessages(final String room,
                                        final String timestamp) {
        return fetchMessages(room, timestamp);
    }

    @Override
    public Mono<Message> postMessage(final String room,
                                     final String sender,
                                     final String language,
                                     final String text) {
        return messageOperations
                .opsForStream()
                .add(Record.of(new MessageItem(sender, language, text)).withStreamKey(toKey(room)))
                .map(id -> toMessage(id, room, sender, language, text));
    }


    private String toKey(final String room) {
        return PREFIX + room;
    }

    private String toRoom(final ObjectRecord<String, MessageItem> record) {
        return record.getStream().substring(PREFIX.length());
    }

    private Message toMessage(final ObjectRecord<String, MessageItem> record) {
        final MessageItem item = record.getValue();
        return toMessage(record.getId(), toRoom(record), item.getSender(), item.getLanguage(), item.getText());
    }

    private Message toMessage(final RecordId id,
                              final String room,
                              final String sender,
                              final String language,
                              final String text) {
        return new RedisMessage(id.getValue(), sender, language, text, id.getTimestamp());
    }

    private static class MessageItem {
        private String sender;
        private String language;
        private String text;

        MessageItem() {
        }

        MessageItem(String sender, String language, String text) {
            this.sender = sender;
            this.language = language;
            this.text = text;
        }

        public String getSender() {
            return sender;
        }

        public void setSender(String sender) {
            this.sender = sender;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
