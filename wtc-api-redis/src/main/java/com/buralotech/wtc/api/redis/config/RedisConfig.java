/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.redis.config;

import com.buralotech.wtc.api.redis.domain.RedisRoom;
import com.buralotech.wtc.api.redis.domain.RedisUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {
    @Bean
    public ReactiveRedisOperations<String, RedisRoom> roomOperations(final ReactiveRedisConnectionFactory factory) {
        return createOperationsForObject(factory, RedisRoom.class);
    }

    @Bean
    public ReactiveRedisOperations<String, RedisUser> userOperations(final ReactiveRedisConnectionFactory factory) {
        return createOperationsForObject(factory, RedisUser.class);
    }

    private <T> ReactiveRedisOperations<String, T> createOperationsForObject(final ReactiveRedisConnectionFactory factory,
                                                                             final Class<T> clazz) {
        final Jackson2JsonRedisSerializer<T> serializer = new Jackson2JsonRedisSerializer<>(clazz);
        final RedisSerializationContext.RedisSerializationContextBuilder<String, T> builder = RedisSerializationContext.newSerializationContext(new StringRedisSerializer());
        final RedisSerializationContext<String, T> context = builder.value(serializer).build();
        return new ReactiveRedisTemplate<>(factory, context);

    }

    @Bean
    public ReactiveRedisOperations<String, String> messageOperations(final ReactiveRedisConnectionFactory factory) {
        return new ReactiveStringRedisTemplate(factory);
    }

}
