/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.redis.service;

import com.buralotech.wtc.api.domain.User;
import com.buralotech.wtc.api.domain.UserFactory;
import com.buralotech.wtc.api.redis.domain.RedisUser;
import com.buralotech.wtc.api.service.database.AbstractUserService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.data.domain.Range;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

@Service
public class RedisUserService extends AbstractUserService {

    private static final String USERS = "users";

    private final ReactiveRedisOperations<String, RedisUser> userOperations;

    RedisUserService(final UserFactory userFactory,
                     final IdentifierService identifierService,
                     final JavaMailSender emailSender,
                     final ReactiveRedisOperations<String, RedisUser> userOperations) {
        super(userFactory, identifierService, emailSender);
        this.userOperations = userOperations;
    }

    @Override
    public Flux<User> fetchUsers() {
        return userOperations
                .opsForZSet()
                .range(USERS, Range.unbounded())
                .cast(User.class);
    }

    @Override
    public Mono<User> createUser(final String name,
                                 final String nickname,
                                 final String emailAddress) {
        return userOperations
                .opsForZSet()
                .range(USERS, Range.unbounded())
                .filter(user -> emailAddress.equals(user.getEmailAddress()) || nickname.equals(user.getNickname()) )
                .next()
                .switchIfEmpty(Mono.defer(() -> doCreate(name, nickname, emailAddress)))
                .cast(User.class);
    }

    private Mono<RedisUser> doCreate(final String name,
                                     final String nickname,
                                     final String emailAddress) {
        return createUserEntity(name, nickname, emailAddress)
                .cast(RedisUser.class)
                .flatMap(user ->
                        userOperations
                                .opsForZSet()
                                .add(USERS, user, 0)
                                .thenReturn(user));
    }

    @Override
    public Mono<User> getUser(final String id) {
        return userOperations
                .opsForZSet()
                .range(USERS, Range.unbounded())
                .filter(user -> id.equalsIgnoreCase(user.getId()))
                .next()
                .cast(User.class);
    }

    @Override
    public Mono<Void> updateUser(final String id,
                                 final String name,
                                 final String nickname,
                                 final String emailAddress) {
        return doUpdate(
                user -> id.equals(user.getId()),
                user -> Mono.just(new RedisUser(id, name, nickname, emailAddress, user.getToken(), user.getPassword())));
    }

    @Override
    public Mono<Void> setUserPassword(final String id,
                                      final String token,
                                      final String newPassword) {
        return doUpdate(
                user -> id.equals(user.getId()) && Objects.equals(token, user.getToken()),
                user -> Mono.just(new RedisUser(id, user.getName(), user.getNickname(), user.getEmailAddress(), null, newPassword)));
    }

    @Override
    public Mono<Void> changeUserPassword(final String id,
                                         final String oldPassword,
                                         final String newPassword) {
        return doUpdate(
                user -> id.equals(user.getId()) && oldPassword.equals(user.getPassword()),
                user -> Mono.just(new RedisUser(id, user.getName(), user.getNickname(), user.getEmailAddress(), null, newPassword)));
    }

    private Mono<Void> doUpdate(final Predicate<RedisUser> predicate,
                                     final Function<RedisUser, Mono<? extends RedisUser>> transform) {
        return userOperations
                .opsForZSet()
                .range(USERS, Range.unbounded())
                .filter(predicate)
                .next()
                .zipWhen(transform)
                .flatMap(tuple ->
                        userOperations
                                .opsForZSet()
                                .remove(USERS, 1, tuple.getT1())
                                .then(userOperations.opsForZSet().add(USERS, tuple.getT2(), 0).then()));
    }

    @Override
    public Mono<Void> deleteUser(final String id) {
        return userOperations
                .opsForZSet()
                .range(USERS, Range.unbounded())
                .filter(user -> id.equalsIgnoreCase(user.getId()))
                .map(user ->
                        userOperations
                                .opsForZSet()
                                .remove(USERS, user))
                .then();
    }
}
