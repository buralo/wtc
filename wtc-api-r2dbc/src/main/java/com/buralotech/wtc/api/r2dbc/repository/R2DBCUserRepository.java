/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.r2dbc.repository;

import com.buralotech.wtc.api.r2dbc.domain.UserImpl;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

public interface R2DBCUserRepository extends R2dbcRepository<UserImpl, String> {

    @Query("SELECT * FROM users WHERE id = $1 AND token = $2")
    Mono<UserImpl> findByIdAndToken(String id, String token);

    @Query("SELECT * FROM users WHERE id = $1 AND password = $2")
    Mono<UserImpl> findByIdAndPassword(String id, String password);
}
