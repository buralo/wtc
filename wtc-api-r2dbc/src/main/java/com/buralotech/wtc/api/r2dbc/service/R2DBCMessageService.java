/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.r2dbc.service;

import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.domain.MessageFactory;
import com.buralotech.wtc.api.r2dbc.domain.MessageImpl;
import com.buralotech.wtc.api.service.database.AbstractMessageService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Clock;

@Service
public class R2DBCMessageService extends AbstractMessageService {

    private final DatabaseClient client;

    private final MessageFactory messageFactory;

    R2DBCMessageService(final DatabaseClient client,
                        final MessageFactory messageFactory,
                        final IdentifierService identifierService,
                        final Clock clock) {
        super(identifierService, clock);
        this.messageFactory = messageFactory;
        this.client = client;
    }

    @Override
    public Mono<Void> createMessageStream(final String room) {
        return Mono.empty();
    }

    @Override
    public Mono<Void> deleteMessageStream(String room) {
        return Mono.empty();
    }

    @Override
    public Flux<Message> fetchMessages(final String room,
                                       final String marker) {
        return client
                .sql("SELECT m.id as id, u.nickname as sender, m.message_language as message_language, m.message_text as message_text, m.message_timestamp as message_timestamp FROM messages as m left outer join users as u ON m.sender = u.id WHERE m.room = $1 AND m.message_timestamp > $2 ORDER BY m.message_timestamp")
                .bind("$1", room)
                .bind("$2", Long.valueOf(marker))
                .mapValue(MessageImpl.class)
                .all()
                .cast(Message.class);
    }

    @Override
    public Mono<Message> postMessage(final String room,
                                     final String sender,
                                     final String language,
                                     final String text) {
        return getIdentifierService()
                .generateIdentifier()
                .map(id -> messageFactory.createMessage(id, sender, language, text, getClock().millis()))
                .flatMap(message ->
                        client
                                .sql("INSERT INTO messages (id, room, sender, message_language, message_text, message_timestamp) VALUES ($1, $2, $3, $4, $5, $6)")
                                .bind("$1", message.getId())
                                .bind("$2", room)
                                .bind("$3", message.getSender())
                                .bind("$4", message.getMessageLanguage())
                                .bind("$5", message.getMessageText())
                                .bind("$6", message.getMessageTimestamp())
                                .fetch()
                                .rowsUpdated()
                                .thenReturn(message));
    }
}
