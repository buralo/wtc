/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.r2dbc.domain;

import com.buralotech.wtc.api.domain.User;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("users")
public class UserImpl implements User {

    @Id
    private final String id;

    private final String name;

    private final String nickname;

    private final String emailAddress;

    private final String token;

    private final String password;

    public UserImpl(final String id,
                    final String name,
                    final String nickname,
                    final String emailAddress,
                    final String token,
                    final String password) {
        this.id = id;
        this.name = name;
        this.nickname = nickname;
        this.emailAddress = emailAddress;
        this.token = token;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNickname() {
        return nickname;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getToken() {
        return token;
    }

    public String getPassword() {
        return password;
    }
}