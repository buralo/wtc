package com.buralotech.wtc.api.r2dbc.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.Resource;
import org.springframework.lang.NonNull;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Component;

import java.io.BufferedInputStream;
import java.io.IOException;

@Component
public class DatabaseInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseInitializer.class);

    private final DatabaseClient databaseClient;

    public DatabaseInitializer(final DatabaseClient databaseClient) {
        this.databaseClient = databaseClient;
    }

    @EventListener
    @Order(50)
    public void onApplicationEvent(@NonNull final ApplicationReadyEvent event) {

        // Load the schema

        final Resource resource = event.getApplicationContext().getResource("classpath:/wtc-schema.sql");
        try (final BufferedInputStream scanner = new BufferedInputStream(resource.getInputStream())) {
            StringBuilder command = new StringBuilder();
            for (int ch = scanner.read(); ch != -1; ch = scanner.read()) {
                if (ch == ';') {
                    execute(command.toString());
                    command = new StringBuilder();
                } else {
                    command.append((char) ch);
                }
            }
            execute(command.toString());
        } catch (final IOException e) {
            LOGGER.error("Unable to load schema resource", e);
        }
    }

    private void execute(final String sql) {
        if (!sql.isEmpty()) {
            databaseClient
                    .sql(sql)
                    .fetch()
                    .rowsUpdated()
                    .doOnSuccess(rows -> LOGGER.info("Execute command: {}", sql))
                    .block();
        }
    }
}
