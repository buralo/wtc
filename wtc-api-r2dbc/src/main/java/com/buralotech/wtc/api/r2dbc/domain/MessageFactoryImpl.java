/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.r2dbc.domain;

import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.domain.MessageFactory;
import org.springframework.stereotype.Component;

@Component
public class MessageFactoryImpl implements MessageFactory {
    @Override
    public Message createMessage(final String id,
                                 final String sender,
                                 final String messageLanguage,
                                 final String messageText,
                                 final long messageTimestamp) {
        return new MessageImpl(id, sender, messageLanguage, messageText, messageTimestamp);
    }
}
