/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.r2dbc.service;

import com.buralotech.wtc.api.domain.User;
import com.buralotech.wtc.api.domain.UserFactory;
import com.buralotech.wtc.api.r2dbc.domain.UserImpl;
import com.buralotech.wtc.api.r2dbc.repository.R2DBCUserRepository;
import com.buralotech.wtc.api.service.database.AbstractUserService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class R2DBCUserService extends AbstractUserService {

    private final R2DBCUserRepository repository;

    private final DatabaseClient databaseClient;

    R2DBCUserService(final UserFactory userFactory,
                     final IdentifierService identifierService,
                     final JavaMailSender emailSender,
                     final R2DBCUserRepository repository,
                     final DatabaseClient databaseClient) {
        super(userFactory, identifierService, emailSender);
        this.repository = repository;
        this.databaseClient = databaseClient;
    }

    @Override
    public Flux<User> fetchUsers() {
        return repository
                .findAll()
                .cast(User.class);
    }

    @Override
    public Mono<User> createUser(final String name,
                                 final String nickname,
                                 final String emailAddress) {
        return createUserEntity(name, nickname, emailAddress)
                .cast(UserImpl.class)
                .flatMap(repository::save)
                .cast(User.class);
    }

    @Override
    public Mono<User> getUser(final String id) {
        return repository.findById(id).cast(User.class);
    }

    @Override
    public Mono<Void> updateUser(final String id,
                                 final String name,
                                 final String nickname,
                                 final String emailAddress) {
        return databaseClient
                .sql("UPDATE users SET name = $2, nickname = $3, emailAddress = $4 WHERE id = $1")
                .bind(0, id)
                .bind(1, name)
                .bind(2, nickname)
                .bind(3, emailAddress)
                .fetch()
                .rowsUpdated()
                .then();
    }

    @Override
    public Mono<Void> setUserPassword(final String id,
                                      final String token,
                                      final String newPassword) {
        return databaseClient
                .sql("UPDATE users SET password = $3, token = null WHERE id = $1 AND token = $2")
                .bind(0, id)
                .bind(1, token)
                .bind(2, newPassword)
                .fetch()
                .rowsUpdated()
                .then();
    }

    @Override
    public Mono<Void> changeUserPassword(final String id,
                                         final String oldPassword,
                                         final String newPassword) {
        return databaseClient
                .sql("UPDATE users SET password = $3, token = null WHERE id = $1 AND password = $2")
                .bind(0, id)
                .bind(1, oldPassword)
                .bind(2, newPassword)
                .fetch()
                .rowsUpdated()
                .then();
    }

    @Override
    public Mono<Void> deleteUser(final String id) {
        return repository.deleteById(id);
    }
}
