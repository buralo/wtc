/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.r2dbc.domain;

import com.buralotech.wtc.api.domain.Message;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("messages")
public class MessageImpl implements Message {

    @Id
    private final String id;

    private final String sender;

    private final String messageLanguage;

    private final String messageText;

    private final long messageTimestamp;

    public MessageImpl(final String id,
                       final String sender,
                       final String messageLanguage,
                       final String messageText,
                       final long messageTimestamp) {
        this.id = id;
        this.sender = sender;
        this.messageLanguage = messageLanguage;
        this.messageText = messageText;
        this.messageTimestamp = messageTimestamp;
    }

    public String getId() {
        return id;
    }

    public String getSender() {
        return sender;
    }

    public String getMessageLanguage() {
        return messageLanguage;
    }

    public String getMessageText() {
        return messageText;
    }

    public long getMessageTimestamp() {
        return messageTimestamp;
    }
}
