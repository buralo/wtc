package com.buralotech.wtc.web.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("wtc.api")
public class ApiProperties {

    private String baseUrl;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(final String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
