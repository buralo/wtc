/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.web.controller;

import com.buralotech.wtc.web.config.ApiProperties;
import com.buralotech.wtc.web.form.ActivateForm;
import com.buralotech.wtc.web.form.LoginForm;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Controller
public class ReactController {

    private final ApiProperties apiProperties;

    private final WebClient client;

    @Autowired
    ReactController(final ApiProperties apiProperties) {
        this.apiProperties = apiProperties;
        this.client = WebClient.builder().baseUrl(apiProperties.getBaseUrl() + "/users").build();
    }

    @GetMapping({"/", "index.html"})
    public Mono<String> showMainPage(final Model model) {
        model.addAttribute("users",
                client.get()
                        .accept(MediaType.APPLICATION_JSON)
                        .retrieve()
                        .bodyToFlux(User.class));
        return Mono.just("index.html");
    }

    @PostMapping({"/", "/index.html"})
    public Mono<String> showPage(final LoginForm loginForm,
                                 final Model model) {
        model.addAttribute("baseUrl", apiProperties.getBaseUrl());
        model.addAttribute("user", loginForm.getUser());
        return Mono.just(loginForm.getView() + "-" + loginForm.getType() + ".html");
    }

    @GetMapping("signup.html")
    public Mono<String> displaySignUp() {
        return Mono.just("signup.html");
    }

    @GetMapping("activate.html")
    public Mono<String> displayActivate(final ActivateForm activateForm,
                                        final Model model) {
        model.addAttribute("form", activateForm);
        return Mono.just("activate.html");
    }

    public static class User {
        private final String id;

        private final String name;

        private final String nickname;

        private final String emailAddress;

        @JsonCreator
        public User(@JsonProperty("id") final String id,
                    @JsonProperty("name") final String name,
                    @JsonProperty("nickname") final String nickname,
                    @JsonProperty("emailAddress") final String emailAddress) {
            this.id = id;
            this.name = name;
            this.nickname = nickname;
            this.emailAddress = emailAddress;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getNickname() {
            return nickname;
        }

        public String getEmailAddress() {
            return emailAddress;
        }
    }
}
