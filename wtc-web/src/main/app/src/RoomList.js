import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Button from 'react-bootstrap/Button';
import ChatRoom from './Chat.js'

export default class RoomList extends Component {
    constructor() {
        super();
        this.state = {
            rooms: []
        };
    }

    componentDidMount() {
        const me = this;
        fetch(this.props.baseUrl + '/rooms', {
            method: 'GET',
            mode: 'cors',
                headers: {'Accept': 'application/json'}
             })
        .then(response => response.json())
        .then(json => me.setState({rooms: json}))
        .catch(error => console.log(error));
    }

    render() {
        return (
            <ul class="list-group">
            {this.state.rooms.map((room) => <RoomListItem baseUrl={this.props.baseUrl} room={room} user={this.props.user}/>)}
            </ul>
        );
    }
}

class RoomListItem extends Component {

    constructor(props) {
        super(props);
        this.switchRoom = this.switchRoom.bind(this);
    }

    render() {
        return (
            <li class="list-group-item">
                <Button variant="link" onClick={this.switchRoom}>{this.props.room.name}</Button>
            </li>
        );
    }

    switchRoom(e) {
        e.preventDefault();
        ReactDOM.render(<ChatRoom baseUrl={this.props.baseUrl} room={this.props.room} user={this.props.user}/>, document.getElementById('chat'));
    }
}