import React, { Component } from 'react';

export class SSEChatService extends Component {
    constructor(props) {
        super(props);
        this.state = {last_timestamp: -1};
        this.eventSource = new EventSource(this.props.baseUrl + '/rooms/' + this.props.room + '/messages')
    }

    componentDidMount() {
        this.eventSource.onmessage = e => this.addChatMessage(JSON.parse(e.data));
    }

    render() {
    }

    addChatMessage(message) {
        if (message.timestamp > this.state.last_timestamp) {
            this.setState({last_timestamp: message.timestamp});
            this.props.onmessage(message)
        }
    }

    postMessage(message) {
        fetch(this.props.baseUrl + '/users/' + this.props.user + '/rooms/' + this.props.room.id + '/messages', {
            mode: 'no-cors',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'text/plain'
            },
            body: message
        }
    }
}