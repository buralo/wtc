import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import RoomList from './RoomList.js'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  render() {
    return (
      <Container>
        <Row>
            <Col lg={3}>
            <RoomList user={this.props.user} baseUrl={this.props.baseUrl}/>
            </Col>
            <Col lg={9}>
            <div id="chat"></div>
            </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
