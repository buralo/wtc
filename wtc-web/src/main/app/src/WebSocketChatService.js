import React, { Component } from 'react';

export class WebSocketChatService extends Component {
    constructor(props) {
        super(props);
        this.state = {last_timestamp: -1, messages: []};
        if (this.props.baseUrl.startsWith('https')) {
            this.webSocket = new WebSocket("ws://localhost:8080/ws/users/" + this.props.user + "/rooms/" + this.props.room.id + "/messages");
        } else {
            this.webSocket = new WebSocket('wss//')
        }
    }

    socketUrl() {
        var baseUrl;
        if (props.baseUrl.startsWith('https')) {
            baseUrl = 'wss' + props.baseUrl.substring(5)
        } else {
        }
        }
    }

    render() {
        return (
            <ul class="messages">
                {this.state.messages.map((message) =>
                    <li class="message">
                        <span class="sender">{message.sender}</span>
                        <span class="timestamp">{message.timestamp}</span>
                        <br />
                        <span class="text">{message.text}</span>
                    </li>
                )}
            </ul>
        );
     }

    componentDidMount() {
        this.eventSource.onmessage = e => this.addChatMessages(JSON.parse(e.data));
    }

    addChatMessages(message) {
        console.log(message);
        if (message.timestamp > this.state.last_timestamp) {
            this.setState({last_timestamp: message.timestamp, messages: [...this.state.messages, message]});
        }
    }
}

export default class WebSocketChatService extends Component {

    constructor(props) {
        super(props);
        this.state = {last_timestamp: -1, messages: []};
        this.eventSource = new EventSource(this.props.baseUrl + '/rooms/' + this.props.room.id + '/messages')
    }

    componentDidMount() {
        this.eventSource.onmessage = e => this.addChatMessages(JSON.parse(e.data));
    }

    postMessage(message) {
        fetch(this.props.baseUrl + '/users/' + this.props.user + '/rooms/' + this.props.room.id + '/messages', {
            mode: 'no-cors',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'text/plain'
            },
            body: message
        }
    }}