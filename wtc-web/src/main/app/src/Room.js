import React, { Component } from 'react';

export default class Room extends Component {
    render() {
        return (
            <div>
            <h2>{this.props.room.name}</h2>
            <MessagesList messagesService={this.props.messagesService} room={this.props.room} />
            <hr />
            <MessageInputForm messagesService={this.props.messagesService} room={this.props.room} user={this.props.user} />
            </div>
        );
    }
}

class MessagesList extends Component {
    constructor(props) {
        super(props);
        this.state = {last_timestamp: -1, messages: []};
        this.eventSource = new EventSource(this.props.baseUrl + '/rooms/' + this.props.room.id + '/messages')
    }

    render() {
        return (
            <ul class="messages">
                {this.state.messages.map((message) =>
                    <li class="message">
                        <span class="sender">{message.sender}</span>
                        <span class="timestamp">{message.timestamp}</span>
                        <br />
                        <span class="text">{message.text}</span>
                    </li>
                )}
            </ul>
        );
     }

    componentDidMount() {
        this.eventSource.onmessage = e => this.addChatMessages(JSON.parse(e.data));
    }

    addChatMessages(message) {
        console.log(message);
        if (message.timestamp > this.state.last_timestamp) {
            this.setState({last_timestamp: message.timestamp, messages: [...this.state.messages, message]});
        }
    }
}

export default class WebSocketChatService extends Component {

    constructor(props) {
        super(props);
        this.state = {last_timestamp: -1, messages: []};
        this.eventSource = new EventSource(this.props.baseUrl + '/rooms/' + this.props.room.id + '/messages')
    }

    componentDidMount() {
        this.eventSource.onmessage = e => this.addChatMessages(JSON.parse(e.data));
    }

    postMessage(message) {
        fetch(this.props.baseUrl + '/users/' + this.props.user + '/rooms/' + this.props.room.id + '/messages', {
            mode: 'no-cors',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'text/plain'
            },
            body: message
        }
    }}