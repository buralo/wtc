package com.buralotech.bot.qotd;

import com.buralotech.wtc.api.payload.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        final SpringApplication application = new SpringApplication(Application.class);
        application.setWebApplicationType(WebApplicationType.NONE);
        application.run(args);
    }

    @Autowired
    private RSocketRequester rSocketRequester;

    @Bean
    public ApplicationRunner loop() {
        return this::loop;
    }

    private void loop(final ApplicationArguments args) {
        final WebClient client = WebClient.builder()
                .baseUrl("http://localhost:8080")
                .build();

        final WebClient quoteClient = WebClient.builder()
                .baseUrl("https://favqs.com")
                .build();

        Flux.interval(Duration.ofSeconds(30))
                .flatMap(t ->
                        quoteClient
                                .get()
                                .uri("/api/qotd")
                                .retrieve()
                                .bodyToMono(QuoteResponse.class)
                )
                .map(QuoteResponse::quote)
                .map(Quote::body)
                .map(quote ->
                        Mono
                                .zip(getOrCreateRoom(client), getOrCreateUser(client))
                                .map(t ->
                                        rSocketRequester
                                                .route(String.format("users.%s.rooms.%s.post", t.getT2(), t.getT1()))
                                                .data(new MessageRequest(quote, "en"))
                                                .send()
                                )
                )
                .blockLast();
    }

    private Mono<String> getOrCreateUser(final WebClient client) {
        return client
                .get()
                .uri("/users")
                .retrieve()
                .bodyToFlux(UserResponse.class)
                .filter(userResponse -> "qotd" .equals(userResponse.nickname()))
                .next()
                .switchIfEmpty(
                        Mono.defer(
                                () -> client.post()
                                        .uri("/users")
                                        .body(BodyInserters.fromObject(new UserRequest("Quote of the Day", "qotd", "qotd@buralo.com")))
                                        .retrieve()
                                        .bodyToMono(UserResponse.class)
                        )
                )
                .map(UserResponse::id);
    }

    private Mono<String> getOrCreateRoom(final WebClient client) {
        return client
                .get()
                .uri("/rooms")
                .retrieve()
                .bodyToFlux(RoomResponse.class)
                .filter(roomResponse -> "General" .equals(roomResponse.name()))
                .next()
                .switchIfEmpty(
                        Mono.defer(
                                () -> client.post()
                                        .uri("/rooms")
                                        .body(BodyInserters.fromObject(new RoomRequest("General", false)))
                                        .retrieve()
                                        .bodyToMono(RoomResponse.class)
                        )
                )
                .map(RoomResponse::id);
    }

    private record QuoteResponse(@JsonProperty("qotd_date") String qotdDate,
                                 Quote quote) {
    }

    private record Quote(int id,
                         boolean dialogue,
                         @JsonProperty("private") boolean privateFlag,
                         String[] tags,
                         String url,
                         @JsonProperty("favorites_count") int favoritesCount,
                         @JsonProperty("upvotes_count")int upvotesCount,
                         @JsonProperty("downvotes_count") int downvotesCount,
                         String author,
                         @JsonProperty("author_permalink") String authorPermalink,
                         String body) {
        }
}