package com.buralotech.bot.qotd.config;

import io.rsocket.RSocket;
import io.rsocket.core.RSocketConnector;
import io.rsocket.frame.decoder.PayloadDecoder;
import io.rsocket.transport.netty.client.TcpClientTransport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.messaging.rsocket.RSocketStrategies;
import org.springframework.util.MimeTypeUtils;

@Configuration
public class BotConfig {

    @Bean
    public RSocketRequester requester(final RSocketStrategies rSocketStrategies) {
        return RSocketRequester.wrap(this.rSocket(), MimeTypeUtils.APPLICATION_JSON, MimeTypeUtils.APPLICATION_JSON, rSocketStrategies);
    }

    @Bean
    public RSocket rSocket() {
        return RSocketConnector.create()
                .payloadDecoder(PayloadDecoder.ZERO_COPY)
                .dataMimeType(MimeTypeUtils.APPLICATION_JSON_VALUE)
                .connect(TcpClientTransport.create(9898))
                .block();
    }
}
