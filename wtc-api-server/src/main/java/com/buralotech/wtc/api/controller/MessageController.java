/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.controller;

import com.buralotech.wtc.api.payload.MessageRequest;
import com.buralotech.wtc.api.payload.MessageResponse;
import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.service.database.MessageService;
import com.buralotech.wtc.api.service.translate.TranslationService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Locale;

@CrossOrigin
@RestController
public class MessageController {

    private final MessageService messageService;

    private final TranslationService translationService;

    private final Converter<Message, MessageResponse> responseConverter;

    MessageController(final MessageService messageService,
                      final TranslationService translationService,
                      final Converter<Message, MessageResponse> responseConverter) {
        this.messageService = messageService;
        this.translationService = translationService;
        this.responseConverter = responseConverter;
    }

    @GetMapping(path = "/rooms/{room}/messages", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<ServerSentEvent<MessageResponse>> streamMessages(@PathVariable final String room,
                                                                 final Locale locale) {
        return messageService
                .streamMessages(room)
                .flatMap(message -> translationService.translate(toLanguage(locale), message))
                .map(responseConverter::convert)
                .map(response -> ServerSentEvent.<MessageResponse>builder()
                        .id(String.valueOf(response.id()))
                        .event("message")
                        .data(response)
                        .build());
    }

    @GetMapping(path = "/rooms/{room}/messages", produces = MediaType.TEXT_EVENT_STREAM_VALUE, params = "marker")
    public Flux<ServerSentEvent<MessageResponse>> streamMessages(@PathVariable final String room,
                                                                 @RequestParam final String marker,
                                                                 final Locale locale) {
        return messageService
                .streamMessages(room, marker)
                .flatMap(message -> translationService.translate(toLanguage(locale), message))
                .map(responseConverter::convert)
                .map(response -> ServerSentEvent.<MessageResponse>builder()
                        .id(String.valueOf(response.id()))
                        .event("message")
                        .data(response)
                        .build());
    }

    @PostMapping(path = "/users/{sender}/rooms/{room}/messages", consumes = MediaType.TEXT_PLAIN_VALUE)
    public Mono<Message> postMessageAsText(@PathVariable final String sender,
                                           @PathVariable final String room,
                                           @RequestBody final Mono<String> request,
                                           final Locale locale) {
        return request
                .flatMap(r -> messageService
                        .postMessage(room, sender, toLanguage(locale), r));
    }

    @PostMapping(path = "/users/{sender}/rooms/{room}/messages", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Message> postMessageAsJSON(@PathVariable final String sender,
                                           @PathVariable final String room,
                                           @RequestBody final Mono<MessageRequest> request) {
        return request
                .flatMap(r -> messageService
                        .postMessage(room, sender, r.language(), r.text()));
    }

    private String toLanguage(final Locale locale) {
        return locale == null ? "en" : locale.getLanguage();
    }
}
