/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.controller;

import com.buralotech.wtc.api.payload.RoomRequest;
import com.buralotech.wtc.api.payload.RoomResponse;
import com.buralotech.wtc.api.domain.Room;
import com.buralotech.wtc.api.service.database.MessageService;
import com.buralotech.wtc.api.service.database.RoomService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin
@RestController
public class RoomController {

    private final RoomService roomService;

    private final MessageService messageService;

    RoomController(final RoomService roomService,
                   final MessageService messageService) {
        this.roomService = roomService;
        this.messageService = messageService;
    }

    @GetMapping("/rooms")
    public Flux<RoomResponse> listRooms() {
        return roomService
                .fetchRooms()
                .map(this::toResponse);
    }

    @PostMapping("/rooms")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<RoomResponse> createRoom(@RequestBody final Mono<RoomRequest> request) {
        return request
                .flatMap(r -> roomService.createRoom(r.name(), r.secret()))
                .flatMap(r -> messageService.createMessageStream(r.getId()).thenReturn(r))
                .map(this::toResponse);
    }

    @GetMapping("/rooms/{room}")
    public Mono<RoomResponse> getRoom(@PathVariable final String room) {
        return roomService
                .getRoom(room)
                .map(this::toResponse);
    }

    @PutMapping("/rooms/{room}")
    public Mono<Void> updateRoom(@PathVariable final String room,
                                 @RequestBody final Mono<RoomRequest> request) {
        return request.flatMap(r -> roomService.updateRoom(room, r.name(), r.secret()));
    }

    @DeleteMapping("/rooms/{room}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> deleteRoom(@PathVariable final String room) {
        return Mono
                .zip(
                        roomService.deleteRoom(room),
                        messageService.deleteMessageStream(room))
                .then();
    }

    private RoomResponse toResponse(final Room room) {
        return new RoomResponse(room.getId(), room.getName(), room.isSecret());
    }
}
