/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.controller;

import com.buralotech.wtc.api.payload.MessageRequest;
import com.buralotech.wtc.api.payload.MessageResponse;
import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.service.database.MessageService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.List;

@Component
public class MessageWebSocketHandler implements WebSocketHandler {

    private final MessageService messageService;

    private final ObjectMapper objectMapper;

    private final Converter<Message, MessageResponse> responseConverter;

    MessageWebSocketHandler(final MessageService messageService,
                            final ObjectMapper objectMapper,
                            final Converter<Message, MessageResponse> responseConverter) {
        this.messageService = messageService;
        this.objectMapper = objectMapper;
        this.responseConverter = responseConverter;
    }

    @Override
    @NonNull
    public Mono<Void> handle(final WebSocketSession webSocketSession) {
        final UriComponents components = UriComponentsBuilder.fromUri(webSocketSession.getHandshakeInfo().getUri()).build();
        final List<String> segments = components.getPathSegments();
        final String sender = segments.get(2);
        final String room = segments.get(4);
        return webSocketSession
                .send(
                        messageService
                                .streamMessages(room)
                                .map(responseConverter::convert)
                                .flatMap(this::toJSON)
                                .map(webSocketSession::textMessage))
                .and(
                        webSocketSession
                                .receive()
                                .map(WebSocketMessage::getPayloadAsText)
                                .flatMap(this::fromJSON)
                                .flatMap(request -> messageService.postMessage(room, sender, request.language(), request.text())));
    }

    private Mono<MessageRequest> fromJSON(final String json) {
        try {
            return Mono.just(objectMapper.readValue(json, MessageRequest.class));
        } catch (final IOException e) {
            return Mono.error(e);
        }
    }

    private Mono<String> toJSON(final MessageResponse message) {
        try {
            return Mono.just(objectMapper.writeValueAsString(message));
        } catch (final JsonProcessingException e) {
            return Mono.error(e);
        }
    }
}
