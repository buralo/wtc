/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.controller;

import com.buralotech.wtc.api.payload.ChangePasswordRequest;
import com.buralotech.wtc.api.payload.SetPasswordRequest;
import com.buralotech.wtc.api.payload.UserRequest;
import com.buralotech.wtc.api.payload.UserResponse;
import com.buralotech.wtc.api.domain.User;
import com.buralotech.wtc.api.service.database.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin
@RestController
public class UserController {

    private final UserService userService;

    UserController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public Flux<UserResponse> listUsers() {
        return userService
                .fetchUsers()
                .map(this::toResponse);
    }

    @PostMapping("/users")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<UserResponse> createUser(@RequestBody final Mono<UserRequest> request) {
        return request
                .flatMap(r -> userService.createUser(r.name(), r.nickname(), r.emailAddress()))
                .map(this::toResponse);
    }

    @GetMapping("/users/{user}")
    public Mono<UserResponse> getUser(@PathVariable final String user) {
        return userService
                .getUser(user)
                .map(this::toResponse);
    }

    @PutMapping("/users/{user}")
    public Mono<Void> updateUser(@PathVariable final String room,
                                 @RequestBody final Mono<UserRequest> request) {
        return request.flatMap(r -> userService.updateUser(room, r.name(), r.nickname(), r.emailAddress()));
    }

    @PutMapping(value = "/users/{user}/password", params = "type=set")
    public Mono<Void> setPassword(@PathVariable final String room,
                                  @RequestBody final Mono<SetPasswordRequest> request) {
        return request.flatMap(r -> userService.setUserPassword(room, r.token(), r.newPassword()));
    }

    @PutMapping(value = "/users/{user}/password", params = "type=change")
    public Mono<Void> changePassword(@PathVariable final String room,
                                     @RequestBody final Mono<ChangePasswordRequest> request) {
        return request.flatMap(r -> userService.setUserPassword(room, r.oldPassword(), r.newPassword()));
    }

    @DeleteMapping("/users/{user}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> deleteUser(@PathVariable final String user) {
        return userService.deleteUser(user);
    }

    private UserResponse toResponse(final User user) {
        return new UserResponse(user.getId(), user.getName(), user.getNickname(), user.getEmailAddress());
    }
}
