/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.config;

import com.buralotech.wtc.api.service.database.MessageService;
import com.buralotech.wtc.api.service.database.RoomService;
import com.buralotech.wtc.api.service.database.UserService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;

@Component
public class DataLoader {

    private final RoomService roomService;

    private final UserService userService;

    private final MessageService messageService;

    public DataLoader(final RoomService roomService,
                      final UserService userService,
                      final MessageService messageService) {
        this.roomService = roomService;
        this.userService = userService;
        this.messageService = messageService;
    }

    @EventListener
    @Order(100)
    public void loadData(@NonNull final ApplicationReadyEvent event) {

        // Populate with sample data

        roomService
                .createRoom("General", false)
                .map(room -> messageService.createMessageStream(room.getId()).thenReturn(room))
                .onErrorResume(ex -> Mono.empty())
                .thenMany(Flux.just(
                        Tuples.of("Brian Matthews", "bmatthews68", "brian@btmatthews.com"),
                        Tuples.of("Yaromir Popov Matthews", "yaromir05", "yaromir@btmatthews.com"),
                        Tuples.of("Elena Popova", "elena77", "elena@btmatthews.com")))
                .flatMap(t -> userService.createUser(t.getT1(), t.getT2(), t.getT3()))
                .onErrorResume(ex -> Mono.empty())
                .flatMap(user -> userService.setUserPassword(user.getId(), user.getToken(), "password"))
                .subscribe();
    }
}
