package com.buralotech.bot.qotd;

import com.buralotech.wtc.api.payload.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        final SpringApplication application = new SpringApplication(Application.class);
        application.setWebApplicationType(WebApplicationType.NONE);
        application.run(args);
    }

    @Bean
    public ApplicationRunner loop() {
        return this::loop;
    }

    private void loop(final ApplicationArguments args) {
        final WebClient client = WebClient.builder()
                .baseUrl("http://localhost:8080")
                .build();

        final WebClient quoteClient = WebClient.builder()
                .baseUrl("https://favqs.com")
                .build();

        Flux.interval(Duration.ofSeconds(30))
                .flatMap(t ->
                        quoteClient
                                .get()
                                .uri("/api/qotd")
                                .retrieve()
                                .bodyToMono(QuoteResponse.class)
                )
                .map(QuoteResponse::getQuote)
                .map(Quote::getBody)
                .map(quote ->
                        Mono.zip(getOrCreateRoom(client), getOrCreateUser(client))
                                .map(t ->
                                        client
                                                .post()
                                                .uri("/users/{sender}/rooms/{room}/messages", t.getT2(), t.getT1())
                                                .body(BodyInserters.fromObject(new MessageRequest(quote, "en")))
                                                .retrieve()
                                                .bodyToMono(MessageResponse.class)
                                        .subscribe()
                                )
                        .subscribe()
                )
                .blockLast();
    }

    private Mono<String> getOrCreateUser(final WebClient client) {
        return client
                .get()
                .uri("/users")
                .retrieve()
                .bodyToFlux(UserResponse.class)
                .filter(userResponse -> "qotd".equals(userResponse.nickname()))
                .next()
                .switchIfEmpty(
                        Mono.defer(
                                () -> client.post()
                                        .uri("/users")
                                        .body(BodyInserters.fromObject(new UserRequest("Quote of the Day", "qotd", "qotd@buralo.com")))
                                        .retrieve()
                                        .bodyToMono(UserResponse.class)
                        )
                )
                .map(UserResponse::id);
    }

    private Mono<String> getOrCreateRoom(final WebClient client) {
        return client
                .get()
                .uri("/rooms")
                .retrieve()
                .bodyToFlux(RoomResponse.class)
                .filter(roomResponse -> "General".equals(roomResponse.name()))
                .next()
                .switchIfEmpty(
                        Mono.defer(
                                () -> client.post()
                                        .uri("/rooms")
                                        .body(BodyInserters.fromObject(new RoomRequest("General", false)))
                                        .retrieve()
                                        .bodyToMono(RoomResponse.class)
                        )
                )
                .map(RoomResponse::id);
    }

    private static class QuoteResponse {
        private final String qotdDate;
        private final Quote quote;

        @JsonCreator
        public QuoteResponse(@JsonProperty("qotd_date") final String qotdDate,
                             @JsonProperty("quote") Quote quote) {
            this.qotdDate = qotdDate;
            this.quote = quote;
        }

        public String getQotdDate() {
            return qotdDate;
        }

        public Quote getQuote() {
            return quote;
        }
    }

    private static class Quote {
        private final int id;
        private final boolean dialogue;
        private final boolean privateFlag;
        private final String[] tags;
        private final String url;
        private final int favoritesCount;
        private final int upvotesCount;
        private final int downvotesCount;
        private final String author;
        private final String authorPermalink;
        private final String body;

        @JsonCreator
        public Quote(@JsonProperty("id") final int id,
                     @JsonProperty("dialogue") final boolean dialogue,
                     @JsonProperty("private") final boolean privateFlag,
                     @JsonProperty("tags") final String[] tags,
                     @JsonProperty("url") final String url,
                     @JsonProperty("favorites_count") final int favoritesCount,
                     @JsonProperty("upvotes_count") final int upvotesCount,
                     @JsonProperty("downvotes_count") final int downvotesCount,
                     @JsonProperty("author") final String author,
                     @JsonProperty("author_permalink") final String authorPermalink,
                     @JsonProperty("body") final String body) {
            this.id = id;
            this.dialogue = dialogue;
            this.privateFlag = privateFlag;
            this.tags = tags;
            this.url = url;
            this.favoritesCount = favoritesCount;
            this.upvotesCount = upvotesCount;
            this.downvotesCount = downvotesCount;
            this.author = author;
            this.authorPermalink = authorPermalink;
            this.body = body;
        }

        public int getId() {
            return id;
        }

        public boolean isDialogue() {
            return dialogue;
        }

        public boolean isPrivateFlag() {
            return privateFlag;
        }

        public String[] getTags() {
            return tags;
        }

        public String getUrl() {
            return url;
        }

        public int getFavoritesCount() {
            return favoritesCount;
        }

        public int getUpvotesCount() {
            return upvotesCount;
        }

        public int getDownvotesCount() {
            return downvotesCount;
        }

        public String getAuthor() {
            return author;
        }

        public String getAuthorPermalink() {
            return authorPermalink;
        }

        public String getBody() {
            return body;
        }
    }

}