package com.buralotech.wtc.api.rsocket.controller;

import com.buralotech.wtc.api.payload.MessageResponse;
import com.buralotech.wtc.api.payload.MessageRequest;
import com.buralotech.wtc.api.rsocket.service.MessageService;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
public class RoomMessagesController {

    private final MessageService messageService;

    RoomMessagesController(final MessageService messageService) {
        this.messageService = messageService;
    }

    @MessageMapping("users.{user}.rooms.{room}.fetch")
    public Flux<MessageResponse> fetchMessages(@DestinationVariable final String user,
                                               @DestinationVariable final String room,
                                               final Integer request) {
        return messageService
                .streamMessages(
                        room,
                        request.longValue()
                );
    }

    @MessageMapping("users.{user}.rooms.{room}.send")
    public Mono<Void> sendMessage(@DestinationVariable final String user,
                                  @DestinationVariable final String room,
                                  final MessageRequest request) {
        return messageService
                .postMessage(
                        room,
                        user,
                        request.text(),
                        request.language()
                )
                .then();
    }

    @MessageMapping("users.{sender}.rooms.{room}.post")
    public Mono<MessageResponse> postMessage(@DestinationVariable final String sender,
                                             @DestinationVariable final String room,
                                             final MessageRequest request) {
        return messageService
                .postMessage(
                        room,
                        sender,
                        request.text(),
                        request.language()
                );
    }

    @MessageMapping("users.{sender}.rooms.{room}.channel")
    public Flux<MessageResponse> roomChannel(@DestinationVariable final String sender,
                                             @DestinationVariable final String room,
                                             final Flux<MessageRequest> request) {
        request.map(message ->
                messageService.postMessage(
                        room,
                        sender,
                        message.text(),
                        message.language()
                )
        ).subscribe();
        return messageService.streamMessages(
                room,
                0L
        );
    }
}