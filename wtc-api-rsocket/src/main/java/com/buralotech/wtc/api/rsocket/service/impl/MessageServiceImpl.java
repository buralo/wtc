package com.buralotech.wtc.api.rsocket.service.impl;

import java.time.Clock;
import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.payload.MessageResponse;
import com.buralotech.wtc.api.r2dbc.domain.MessageImpl;
import com.buralotech.wtc.api.r2dbc.repository.R2DBCMessageRepository;
import com.buralotech.wtc.api.rsocket.service.MessageService;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MessageServiceImpl implements MessageService {

    private static final String SELECT_SQL = """
            SELECT m.id AS id, \
            m.room AS room, \
            u.nickname AS sender, \
            m.message_language AS message_language, \
            m.message_text AS message_text, \
            m.message_timestamp AS message_timestamp \
            FROM messages AS m \
            LEFT OUTER JOIN users AS u ON m.sender = u.id \
            WHERE m.room = $1 \
            AND m.message_timestamp > $2 \
            ORDER BY m.message_timestamp
            """;

    private final DatabaseClient client;

    private final R2DBCMessageRepository repository;

    private final Clock clock;

    public MessageServiceImpl(final DatabaseClient client,
                              final R2DBCMessageRepository repository,
                              final Clock clock) {
        this.client = client;
        this.repository = repository;
        this.clock = clock;
    }

    @Override
    public Flux<MessageResponse> streamMessages(String room, long timestamp) {
        final AtomicLong last = new AtomicLong(timestamp);
        return Flux.interval(Duration.ZERO, Duration.ofSeconds(1))
                .zipWith(Flux.<AtomicLong>generate(generator -> generator.next(last)))
                .flatMap(s -> fetchMessages(room, s.getT2().longValue())
                        .doOnNext(message -> s.getT2().set(message.timestamp()))
                );
    }

    private Flux<MessageResponse> fetchMessages(final String room,
                                                final long timestamp) {
        return client
                .sql(SELECT_SQL)
                .bind("$1", room)
                .bind("$2", timestamp)
                .mapValue(MessageImpl.class)
                .all()
                .map(this::convert);
    }

    @Override
    public Mono<MessageResponse> postMessage(final String room,
                                             final String sender,
                                             final String text,
                                             final String language) {
        final MessageImpl message = new MessageImpl(
                UUID.randomUUID().toString(),
                sender,
                language,
                text,
                clock.millis());
        return repository
                .save(message)
                .map(result -> convert(message));
    }

    private MessageResponse convert(final Message message) {
        return new MessageResponse(
                message.getId(),
                message.getSender(),
                message.getMessageText(),
                message.getMessageLanguage(),
                message.getMessageTimestamp(),
                message.getMarker());
    }
}
