package com.buralotech.wtc.api.rsocket.configuration;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;

@Configuration
@EnableConfigurationProperties(PostgresProperties.class)
public class DBConfig extends AbstractR2dbcConfiguration {

    private final PostgresProperties properties;

    DBConfig(final PostgresProperties properties) {
        this.properties = properties;
    }

    @Override
    public ConnectionFactory connectionFactory() {
        return new PostgresqlConnectionFactory(
                PostgresqlConnectionConfiguration
                        .builder()
                        .applicationName("wtc")
                        .host(properties.getHost())
                        .port(properties.getPort())
                        .database(properties.getDatabase())
                        .schema(properties.getSchema())
                        .username(properties.getUsername())
                        .password(properties.getPassword())
                        .build()
        );
    }
}
