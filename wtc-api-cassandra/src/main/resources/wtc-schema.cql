/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

CREATE KEYSPACE IF NOT EXISTS wtc
    WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };

USE wtc;

CREATE TABLE IF NOT EXISTS messages (
    room text,
    message_timestamp timeuuid,
    sender text,
    message_text text,
    message_language text,
    PRIMARY KEY (room, message_timestamp)
)
WITH CLUSTERING ORDER BY (message_timestamp ASC);

CREATE TABLE IF NOT EXISTS rooms (
    id text,
    name text,
    secret boolean,
    PRIMARY KEY (id)
);

CREATE INDEX IF NOT EXISTS rooms_name_idx
    ON rooms (name);

CREATE TABLE IF NOT EXISTS occupants (
    room_id text,
    user_id text,
    role text,
    PRIMARY KEY (room_id, user_id, role)
);

CREATE TABLE IF NOT EXISTS users (
    id text,
    name text,
    nickname text,
    email_address text,
    password text,
    password_token text,
    PRIMARY KEY (id)
);

CREATE INDEX IF NOT EXISTS users_nickname_idx
    ON users (nickname);

CREATE INDEX IF NOT EXISTS users_email_idx
    ON users (email_address);


INSERT INTO wtc.rooms (id, name, secret) VALUES ('ac02acf8-6d62-4f5b-9aec-7175451ab6fb', 'General', false);
INSERT INTO wtc.rooms (id, name, secret) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', 'Matthews Family', true);
INSERT INTO wtc.users (id, name, nickname, email_address, password) VALUES ('912e86f0-2e38-4987-bc10-8bb1930173a6', 'Brian Matthews', 'bmatthews68', 'brian@buralotech.com', 'password');
INSERT INTO wtc.users (id, name, nickname, email_address, password) VALUES ('7b40e339-1a3e-4c03-bed1-8ab1749d95f1', 'Elena Popova', 'elena77', 'elena@buralotech.com', 'password');
INSERT INTO wtc.users (id, name, nickname, email_address, password) VALUES ('5071f75c-5e49-4ef4-be4b-e651534fc57c', 'Yaromir Popov Matthews', 'yaromir05', 'yaromir@buralotech.com', 'password');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '912e86f0-2e38-4987-bc10-8bb1930173a6', 'OWNER');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '912e86f0-2e38-4987-bc10-8bb1930173a6', 'MODERATOR');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '912e86f0-2e38-4987-bc10-8bb1930173a6', 'CONTRIBUTOR');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '7b40e339-1a3e-4c03-bed1-8ab1749d95f1', 'MODERATOR');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '7b40e339-1a3e-4c03-bed1-8ab1749d95f1', 'CONTRIBUTOR');
INSERT INTO wtc.occupants (room_id, user_id, role) VALUES ('984d010d-1325-48f2-b8b8-aa55f7741d2e', '5071f75c-5e49-4ef4-be4b-e651534fc57c', 'CONTRIBUTOR');