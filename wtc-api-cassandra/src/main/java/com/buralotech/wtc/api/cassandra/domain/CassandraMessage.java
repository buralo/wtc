/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.cassandra.domain;

import com.buralotech.wtc.api.domain.Message;
import com.datastax.oss.driver.api.core.uuid.Uuids;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

/**
 *
 */
@Table("messages")
public class CassandraMessage implements Message {

    /**
     * The message belongs to the room.
     */
    @PrimaryKeyColumn(ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private String room;

    /**
     * The message sender is mapped to a column called sender.
     */
    private String sender;

    /**
     * The message language is mapped to a column called message_language.
     */
    @Column("message_language")
    private String messageLanguage;

    /**
     * The message text is mapped to a column called message_text.
     */
    @Column("message_text")
    private String messageText;

    /**
     *
     */
    @PrimaryKeyColumn(name = "message_timestamp", ordinal = 1)
    private UUID messageTimestamp;

    /**
     *
     * @param room
     * @param sender
     * @param messageLanguage
     * @param messageText
     * @param messageTimestamp
     */
    public CassandraMessage(final String room,
                            final String sender,
                            final String messageLanguage,
                            final String messageText,
                            final UUID messageTimestamp) {
        this.room = room;
        this.sender = sender;
        this.messageLanguage = messageLanguage;
        this.messageText = messageText;
        this.messageTimestamp = messageTimestamp;
    }

    /**
     * Get the message identifier.
     *
     * @return The message identifier.
     */
    @Override
    public String getId() {
        return messageTimestamp.toString();
    }

    /**
     * Get the room identifier.
     *
     * @return The room identifier.
     */
    public String getRoom() {
        return room;
    }

    /**
     * Get the sender identifier.
     *
     * @return The sender identifier.
     */
    @Override
    public String getSender() {
        return sender;
    }

    /**
     * Get the language code.
     *
     * @return The language code.
     */
    @Override
    public String getMessageLanguage() {
        return messageLanguage;
    }

    /**
     * Get the text.
     *
     * @return The text.
     */
    @Override
    public String getMessageText() {
        return messageText;
    }

    /**
     * Get the timestamp.
     *
     * @return The timestamp.
     */
    @Override
    public long getMessageTimestamp() {
        return Uuids.unixTimestamp(messageTimestamp);
    }
}
