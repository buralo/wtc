/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.cassandra.service;

import com.buralotech.wtc.api.cassandra.domain.CassandraUser;
import com.buralotech.wtc.api.cassandra.repository.UserRepository;
import com.buralotech.wtc.api.domain.User;
import com.buralotech.wtc.api.domain.UserFactory;
import com.buralotech.wtc.api.service.database.AbstractUserService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CassandraUserService extends AbstractUserService {

    private final UserRepository repository;

    CassandraUserService(final UserFactory userFactory,
                         final IdentifierService identifierService,
                         final JavaMailSender emailSender,
                         final UserRepository repository) {
        super(userFactory, identifierService, emailSender);
        this.repository = repository;
    }

    @Override
    public Flux<User> fetchUsers() {
        return repository
                .findAll()
                .cast(User.class);
    }

    @Override
    public Mono<User> createUser(final String name,
                                 final String nickname,
                                 final String emailAddress) {
        return Mono
                .zip(repository.existsByNickname(nickname), repository.existsByEmailAddress(emailAddress))
                .flatMap(t -> t.getT1() || t.getT2() ? reportDuplicateUser(t.getT1(), t.getT2(), nickname, emailAddress) : doCreateUser(name, nickname, emailAddress));
    }

    private Mono<User> reportDuplicateUser(final boolean duplicateNickname,
                                           final boolean duplicateEmailAddress,
                                           final String nickname,
                                           final String emailAddress) {
        if (duplicateNickname) {
            if (duplicateEmailAddress) {
                return Mono.error(new IllegalStateException("User with nickname " + nickname + " and email address " + emailAddress + " already exists"));

            } else {
                return Mono.error(new IllegalStateException("User with nickname " + nickname + " already exists"));
            }
        } else {
            return Mono.error(new IllegalStateException("User with email address " + emailAddress + " already exists"));
        }
    }

    private Mono<User> doCreateUser(final String name,
                                    final String nickname,
                                    final String emailAddress) {
        return createUserEntity(name, nickname, emailAddress)
                .cast(CassandraUser.class)
                .flatMap(repository::insert)
                .cast(User.class);
    }


    @Override
    public Mono<User> getUser(final String id) {
        return repository
                .findById(id)
                .cast(User.class);
    }

    @Override
    public Mono<Void> updateUser(final String id,
                                 final String name,
                                 final String nickname,
                                 final String emailAddress) {
        return repository
                .findById(id)
                .map(user -> new CassandraUser(id, name, nickname, emailAddress, user.getToken(), user.getPassword()))
                .flatMap(repository::save)
                .then();
    }

    @Override
    public Mono<Void> setUserPassword(final String id,
                                      final String token,
                                      final String newPassword) {
        return repository
                .findById(id)
                .filter(user -> user.getToken().equals(token))
                .map(user -> new CassandraUser(id, user.getName(), user.getNickname(), user.getEmailAddress(), null, newPassword))
                .map(repository::save)
                .then();
    }

    @Override
    public Mono<Void> changeUserPassword(final String id,
                                         final String oldPassword,
                                         final String newPassword) {
        return repository
                .findById(id)
                .filter(user -> user.getPassword().equals(oldPassword))
                .map(user -> new CassandraUser(id, user.getName(), user.getNickname(), user.getEmailAddress(), null, newPassword))
                .map(repository::save)
                .then();
    }

    @Override
    public Mono<Void> deleteUser(final String id) {
        return repository.deleteById(id);
    }
}
