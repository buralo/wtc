/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.cassandra.domain;

import com.buralotech.wtc.api.domain.Room;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

/**
 *
 */
@Table("rooms")
public class CassandraRoom implements Room {

    /**
     *
     */
    @PrimaryKey
    private String id;

    /**
     *
     */
    @Indexed("rooms_name_idx")
    private String name;

    /**
     *
     */
    private boolean secret;

    /**
     *
     */
    public CassandraRoom() {
    }

    /**
     *
     * @param id
     * @param name
     * @param secret
     */
    public CassandraRoom(final String id,
                         final String name,
                         final boolean secret) {
        this.id = id;
        this.name = name;
        this.secret = secret;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public boolean isSecret() {
        return secret;
    }

    public void setSecret(final boolean secret) {
        this.secret = secret;
    }
}
