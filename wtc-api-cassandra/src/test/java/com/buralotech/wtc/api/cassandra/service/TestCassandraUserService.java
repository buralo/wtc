package com.buralotech.wtc.api.cassandra.service;

import com.buralotech.wtc.api.domain.User;
import com.buralotech.wtc.api.service.id.uuid.UUIDIdentifierService;
import com.buralotech.wtc.api.cassandra.config.CassandraConfig;
import com.buralotech.wtc.api.cassandra.domain.CassandraUserFactory;
import com.buralotech.wtc.api.cassandra.repository.UserRepository;
import net.datafaker.Faker;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.cassandra.DataCassandraTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ContextConfiguration;
import reactor.test.StepVerifier;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.atIndex;

@DataCassandraTest(properties = {
        "spring.cassandra.keyspace-name=wtc"
})
@EnableAutoConfiguration
@ContextConfiguration(classes = CassandraConfig.class)
@Import(ContainersConfig.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TestCassandraUserService {

    private final static Faker FAKER = new Faker();

    private static final String USER1_ID = "912e86f0-2e38-4987-bc10-8bb1930173a6";

    private static final String USER1_NAME = "Brian Matthews";

    private static final String USER1_NICKNAME = "bmatthews68";

    private static final String USER1_EMAIL = "brian@buralotech.com";

    private static final String WRONG_PASSWORD = "private";

    private static final String PASSWORD = "password";

    private static final String NEW_PASSWORD = "secret";

    private static final String USER2_ID = "7b40e339-1a3e-4c03-bed1-8ab1749d95f1";

    private static final String USER2_NAME = "Elena Popova";

    private static final String USER2_NICKNAME = "elena77";

    private static final String USER2_EMAIL = "elena@buralotech.com";

    private static final String USER3_ID = "5071f75c-5e49-4ef4-be4b-e651534fc57c";

    private static final String USER3_NAME = "Yaromir Popov Matthews";

    private static final String USER3_NICKNAME = "yaromir05";

    private static final String USER3_EMAIL = "yaromir@buralotech.com";

    @Autowired
    private UserRepository repository;

    @MockBean
    private JavaMailSender emailSender;

    private CassandraUserService service;

    @BeforeEach
    void setup() {
        service = new CassandraUserService(
                new CassandraUserFactory(),
                new UUIDIdentifierService(),
                emailSender,
                repository);
    }

    @Order(1)
    @Test
    void fetchUsersFromNonEmptyTable() {
        final var users = new LinkedList<User>();
        StepVerifier.create(service.fetchUsers())
                .consumeNextWith(users::add)
                .consumeNextWith(users::add)
                .consumeNextWith(users::add)
                .expectComplete()
                .verify();
        users.sort(Comparator.comparing(User::getNickname));
        assertThat(users)
                .satisfies(user -> {
                    assertThat(user.getId()).isEqualTo(USER1_ID);
                    assertThat(user.getName()).isEqualTo(USER1_NAME);
                    assertThat(user.getNickname()).isEqualTo(USER1_NICKNAME);
                    assertThat(user.getEmailAddress()).isEqualTo(USER1_EMAIL);
                    assertThat(user.getPassword()).isEqualTo(PASSWORD);
                    assertThat(user.getToken()).isNull();
                }, atIndex(0))
                .satisfies(user -> {
                    assertThat(user.getId()).isEqualTo(USER2_ID);
                    assertThat(user.getName()).isEqualTo(USER2_NAME);
                    assertThat(user.getNickname()).isEqualTo(USER2_NICKNAME);
                    assertThat(user.getEmailAddress()).isEqualTo(USER2_EMAIL);
                    assertThat(user.getPassword()).isEqualTo(PASSWORD);
                    assertThat(user.getToken()).isNull();
                }, atIndex(1))
                .satisfies(user -> {
                    assertThat(user.getId()).isEqualTo(USER3_ID);
                    assertThat(user.getName()).isEqualTo(USER3_NAME);
                    assertThat(user.getNickname()).isEqualTo(USER3_NICKNAME);
                    assertThat(user.getEmailAddress()).isEqualTo(USER3_EMAIL);
                    assertThat(user.getPassword()).isEqualTo(PASSWORD);
                    assertThat(user.getToken()).isNull();
                }, atIndex(2));
    }

    @Order(2)
    @Test
    void canCreateNewUser() {
        final var name = FAKER.name().fullName();
        final var nickname = FAKER.internet().username();
        final var emailAddress = FAKER.internet().emailAddress();
        StepVerifier.create(service.createUser(name, nickname, emailAddress))
                .assertNext(user -> {
                    assertThat(user.getId()).isNotNull();
                    assertThat(user.getName()).isEqualTo(name);
                    assertThat(user.getNickname()).isEqualTo(nickname);
                    assertThat(user.getEmailAddress()).isEqualTo(emailAddress);
                    assertThat(user.getPassword()).isNull();
                    assertThat(user.getToken()).isNotNull();
                })
                .expectComplete()
                .verify();
    }

    @Order(3)
    @Test
    void cannotCreateNewUserWithDuplicateEmail() {
        final var name = FAKER.name().fullName();
        final var nickname = FAKER.internet().username();
        StepVerifier.create(service.createUser(name, nickname, USER1_EMAIL))
                .expectErrorSatisfies(error -> assertThat(error).isInstanceOf(IllegalStateException.class).hasMessage("User with email address brian@buralotech.com already exists"))
                .verify();
    }

    @Order(4)
    @Test
    void cannotCreateNewUserWithDuplicateNickname() {
        final var name = FAKER.name().fullName();
        final var emailAddress = FAKER.internet().emailAddress();
        StepVerifier.create(service.createUser(name, USER1_NICKNAME, emailAddress))
                .expectErrorSatisfies(error -> assertThat(error).isInstanceOf(IllegalStateException.class).hasMessage("User with nickname bmatthews68 already exists"))
                .verify();
    }

    @Order(5)
    @Test
    void cannotCreateNewUserWithDuplicateNicknameAndEmail() {
        final var name = FAKER.name().fullName();
        StepVerifier.create(service.createUser(name, USER1_NICKNAME, USER1_EMAIL))
                .expectErrorSatisfies(error -> assertThat(error).isInstanceOf(IllegalStateException.class).hasMessage("User with nickname bmatthews68 and email address brian@buralotech.com already exists"))
                .verify();
    }

    @Order(6)
    @Test
    void canGetUserThatExists() {
        StepVerifier.create(service.getUser(USER1_ID))
                .assertNext(user -> {
                    assertThat(user.getId()).isEqualTo(USER1_ID);
                    assertThat(user.getName()).isEqualTo(USER1_NAME);
                    assertThat(user.getNickname()).isEqualTo(USER1_NICKNAME);
                    assertThat(user.getEmailAddress()).isEqualTo(USER1_EMAIL);
                    assertThat(user.getPassword()).isEqualTo(PASSWORD);
                    assertThat(user.getToken()).isNull();
                })
                .expectComplete()
                .verify();
    }

    @Order(7)
    @Test
    void cannotGetUserThatDoesNotExist() {
        final var id = UUID.randomUUID().toString();
        StepVerifier.create(service.getUser(id))
                .expectComplete()
                .verify();
    }

    @Order(8)
    @Test
    void canUpdateUserThatExists() {
        final var name = FAKER.name().fullName();
        final var nickname = FAKER.internet().username();
        final var emailAddress = FAKER.internet().emailAddress();
        StepVerifier.create(service.updateUser(USER3_ID, name, nickname, emailAddress))
                .expectComplete()
                .verify();
        StepVerifier.create(service.getUser(USER3_ID))
                .assertNext(user -> {
                    assertThat(user.getId()).isEqualTo(USER3_ID);
                    assertThat(user.getName()).isEqualTo(name);
                    assertThat(user.getNickname()).isEqualTo(nickname);
                    assertThat(user.getEmailAddress()).isEqualTo(emailAddress);
                    assertThat(user.getPassword()).isEqualTo(PASSWORD);
                    assertThat(user.getToken()).isNull();
                })
                .expectComplete()
                .verify();
    }

    @Order(9)
    @Test
    void cannotChangePasswordIfOldPasswordIsWrong() {
        StepVerifier.create(service.changeUserPassword(USER1_ID, WRONG_PASSWORD, NEW_PASSWORD))
                .expectComplete()
                .verify();

        StepVerifier.create(service.getUser(USER1_ID))
                .assertNext(user -> {
                    assertThat(user.getId()).isEqualTo(USER1_ID);
                    assertThat(user.getName()).isEqualTo(USER1_NAME);
                    assertThat(user.getNickname()).isEqualTo(USER1_NICKNAME);
                    assertThat(user.getEmailAddress()).isEqualTo(USER1_EMAIL);
                    assertThat(user.getPassword()).isEqualTo(PASSWORD);
                    assertThat(user.getToken()).isNull();
                })
                .expectComplete()
                .verify();
    }

    @Order(10)
    @Test
    void canChangePasswordIfOldPasswordIsCorrect() {
        StepVerifier.create(service.changeUserPassword(USER1_ID, PASSWORD, NEW_PASSWORD))
                .expectComplete()
                .verify();

        StepVerifier.create(service.getUser(USER1_ID))
                .assertNext(user -> {
                    assertThat(user.getId()).isEqualTo(USER1_ID);
                    assertThat(user.getName()).isEqualTo(USER1_NAME);
                    assertThat(user.getNickname()).isEqualTo(USER1_NICKNAME);
                    assertThat(user.getEmailAddress()).isEqualTo(USER1_EMAIL);
                    assertThat(user.getPassword()).isEqualTo(NEW_PASSWORD);
                    assertThat(user.getToken()).isNull();
                })
                .expectComplete()
                .verify();
    }

    @Order(11)
    @Test
    void canDeleteAnUserThatExists() {
        StepVerifier.create(service.deleteUser(USER1_ID))
                .expectComplete()
                .verify();

        StepVerifier.create(service.getUser(USER1_ID))
                .expectComplete()
                .verify();
    }

    @Order(12)
    @Test
    void cannotDeleteAnUserThatDoesNotExistWithoutError() {
        final var id = UUID.randomUUID().toString();
        StepVerifier.create(service.deleteUser(id))
                .expectComplete()
                .verify();
    }
}
