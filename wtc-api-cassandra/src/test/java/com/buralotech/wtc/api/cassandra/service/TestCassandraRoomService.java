package com.buralotech.wtc.api.cassandra.service;

import com.buralotech.wtc.api.domain.Room;
import com.buralotech.wtc.api.service.id.uuid.UUIDIdentifierService;
import com.buralotech.wtc.api.cassandra.config.CassandraConfig;
import com.buralotech.wtc.api.cassandra.repository.RoomRepository;
import net.datafaker.Faker;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.cassandra.DataCassandraTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ContextConfiguration;
import reactor.test.StepVerifier;

import java.util.Comparator;
import java.util.LinkedList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.atIndex;

@DataCassandraTest(properties = {
        "spring.cassandra.keyspace-name=wtc"
})
@EnableAutoConfiguration
@ContextConfiguration(classes = CassandraConfig.class)
@Import(ContainersConfig.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TestCassandraRoomService {

    private final static Faker FAKER = new Faker();

    @Autowired
    private RoomRepository repository;

    @MockBean
    private JavaMailSender emailSender;

    private CassandraRoomService service;

    @BeforeEach
    void setup() {
        service = new CassandraRoomService(
                new UUIDIdentifierService(),
                repository);
    }

    @Order(1)
    @Test
    void canFetchRooms() {
        final var rooms = new LinkedList<Room>();
        StepVerifier.create(service.fetchRooms())
                .consumeNextWith(rooms::add)
                .consumeNextWith(rooms::add)
                .expectComplete()
                .verify();
        rooms.sort(Comparator.comparing(Room::getName));
        assertThat(rooms)
                .satisfies(room -> {
                    assertThat(room.getId()).isEqualTo("ac02acf8-6d62-4f5b-9aec-7175451ab6fb");
                    assertThat(room.getName()).isEqualTo("General");
                    assertThat(room.isSecret()).isFalse();
                }, atIndex(0))
                .satisfies(room -> {
                    assertThat(room.getId()).isEqualTo("984d010d-1325-48f2-b8b8-aa55f7741d2e");
                    assertThat(room.getName()).isEqualTo("Matthews Family");
                    assertThat(room.isSecret()).isTrue();
                }, atIndex(1));
    }

    @Order(2)
    @Test
    void canCreatePrivateRoom() {
        final var name = FAKER.funnyName().name();
        StepVerifier.create(service.createRoom(name, true))
                .assertNext(room -> {
                    assertThat(room.getId()).isNotNull();
                    assertThat(room.getName()).isEqualTo(name);
                    assertThat(room.isSecret()).isTrue();
                });
    }

    @Order(2)
    @Test
    void canCreatePublicRoom() {
        final var name = FAKER.funnyName().name();
        StepVerifier.create(service.createRoom(name, true))
                .assertNext(room -> {
                    assertThat(room.getId()).isNotNull();
                    assertThat(room.getName()).isEqualTo(name);
                    assertThat(room.isSecret()).isFalse();
                });
    }
}
