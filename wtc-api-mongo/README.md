```
db.createCollection("rooms")
db.rooms.createIndex({rooms: 1}, {unique: true})
db.rooms.insertOne({_id: "dc3be394-8711-11e9-bc42-526af7764f64", name: "General", secret: false, _class: "domain.com.buralotech.wtc.api.mongo.RoomImpl"})
db.createCollection("users")
db.users.createIndex({nickname: 1}, {unique: true})
db.users.createIndex({emailAddress: 1}, {unique: true})
db.users.insertOne({_id: "dc3be600-8711-11e9-bc42-526af7764f64", name: "WTC Bot", nickname: "wtcbot", emailAddress: "wtcbot@buralo.com", token: "88ee5105-b906-4d1e-9f1b-caa390f076ac", _class: "domain.com.buralotech.wtc.api.mongo.UserImpl"})
db.users.insertOne({_id: "dc3bef06-8711-11e9-bc42-526af7764f64", name: "Brian Matthews", nickname: "bmatthews68", emailAddress: "brian@btmatthews.com", token: "88ee5105-b906-4d1e-9f1b-caa390f076ac", _class: "domain.com.buralotech.wtc.api.mongo.UserImpl"})
db.users.insertOne({_id: "dc3bf06e-8711-11e9-bc42-526af7764f64", name: "Yaromir  Popov Matthews", nickname: "yaromir05", emailAddress: "yaromir@btmatthews.com", token: "677c94d7-dc00-446f-a16c-41f7fc1e3a0f", _class: "domain.com.buralotech.wtc.api.mongo.UserImpl"})
db.users.insertOne({_id: "dc3bf19a-8711-11e9-bc42-526af7764f64", name: "Elena Popova", nickname: "elena77", emailAddress: "elena@btmatthews.com", token: "b36dce1d-71da-4931-b2d1-d05a1b0582ea", _class: "domain.com.buralotech.wtc.api.mongo.UserImpl"})
db.createCollection("messages_dc3be394871111e9bc42526af7764f64", {capped: true, size: 1048576, max: 5000})
```