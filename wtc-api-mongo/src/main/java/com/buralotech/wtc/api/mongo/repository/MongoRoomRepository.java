/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.mongo.repository;

import com.buralotech.wtc.api.mongo.domain.RoomImpl;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface MongoRoomRepository extends ReactiveMongoRepository<RoomImpl, String> {

    @Query("{_id: ?0, $set: {'name': ?1, 'secret': ?2 }}")
    Mono<Void> update(String id, String name, boolean secret);
}
