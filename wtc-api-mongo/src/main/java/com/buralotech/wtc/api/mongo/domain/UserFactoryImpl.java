/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.mongo.domain;

import com.buralotech.wtc.api.domain.User;
import com.buralotech.wtc.api.domain.UserFactory;
import org.springframework.stereotype.Component;

@Component
public class UserFactoryImpl implements UserFactory {
    @Override
    public User createUser(final String id,
                           final String name,
                           final String nickname,
                           final String emailAddress,
                           final String token,
                           final String password) {
        return new UserImpl(id, name, nickname, emailAddress, token, password);
    }
}
