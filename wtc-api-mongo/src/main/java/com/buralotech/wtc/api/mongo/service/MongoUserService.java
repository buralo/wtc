/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.mongo.service;

import com.buralotech.wtc.api.domain.User;
import com.buralotech.wtc.api.domain.UserFactory;
import com.buralotech.wtc.api.mongo.domain.UserImpl;
import com.buralotech.wtc.api.mongo.repository.MongoUserRepository;
import com.buralotech.wtc.api.service.database.AbstractUserService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.data.domain.Sort;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MongoUserService extends AbstractUserService {

    private final MongoUserRepository repository;

    MongoUserService(final UserFactory userFactory,
                     final IdentifierService identifierService,
                     final JavaMailSender javaMailSender,
                     final MongoUserRepository repository) {
        super(userFactory, identifierService, javaMailSender);
        this.repository = repository;
    }

    @Override
    public Flux<User> fetchUsers() {
        return repository
                .findAll(Sort.by("nickname"))
                .cast(User.class);
    }

    @Override
    public Mono<User> createUser(final String name,
                                 final String nickname,
                                 final String emailAddress) {
        return createUserEntity(name, nickname, emailAddress)
                .cast(UserImpl.class)
                .flatMap(repository::insert);
    }

    @Override
    public Mono<User> getUser(final String id) {
        return repository
                .findById(id)
                .cast(User.class);
    }

    @Override
    public Mono<Void> updateUser(final String id,
                                 final String name,
                                 final String nickname,
                                 final String emailAddress) {
        return repository
                .findById(id)
                .map(user -> getUserFactory().createUser(
                        id,
                        name,
                        nickname,
                        emailAddress,
                        user.getToken(),
                        user.getPassword()))
                .cast(UserImpl.class)
                .map(repository::save)
                .then();
    }

    @Override
    public Mono<Void> setUserPassword(final String id,
                                      final String token,
                                      final String newPassword) {
        return repository.findById(id)
                .map(user -> getUserFactory().createUser(
                        id,
                        user.getName(),
                        user.getNickname(),
                        user.getEmailAddress(),
                        null,
                        newPassword))
                .cast(UserImpl.class)
                .map(repository::save)
                .then();
    }

    @Override
    public Mono<Void> changeUserPassword(final String id,
                                         final String oldPassword,
                                         final String newPassword) {
        return repository
                .findById(id)
                .filter(user -> oldPassword.equals(user.getPassword()))
                .map(user -> getUserFactory().createUser(
                        id,
                        user.getName(),
                        user.getNickname(),
                        user.getEmailAddress(),
                        null,
                        newPassword))
                .cast(UserImpl.class)
                .map(repository::save)
                .then();
    }

    @Override
    public Mono<Void> deleteUser(final String id) {
        return repository.deleteById(id);
    }
}
