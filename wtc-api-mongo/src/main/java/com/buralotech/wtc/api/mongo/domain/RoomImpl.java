/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.mongo.domain;

import com.buralotech.wtc.api.domain.Room;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("rooms")
public class RoomImpl implements Room {

    @Id
    private final String id;

    private final String name;

    private final Boolean secret;

    private final List<Occupant> occupants;

    public RoomImpl(final String id,
                    final String name,
                    final Boolean secret,
                    final List<Occupant> occupants) {
        this.id = id;
        this.name = name;
        this.secret = secret;
        this.occupants = occupants;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isSecret() {
        return secret;
    }

    public List<Occupant> getOccupants() {
        return occupants;
    }

    public static class Occupant {
        private String userId;
        private List<String> roles;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public List<String> getRoles() {
            return roles;
        }

        public void setRoles(List<String> roles) {
            this.roles = roles;
        }
    }
}
