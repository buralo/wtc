/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.mongo.repository;

import com.buralotech.wtc.api.mongo.domain.UserImpl;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface MongoUserRepository extends ReactiveMongoRepository<UserImpl, String> {

//    @Query("UPDATE wtc.users SET name = $2, nickname = $3, emailAddress = $4 WHERE id = $1")
//    Mono<Void> updateProfile(String id, String name, String nickname, String emailAddress);

//    @Query("UPDATE wtc.users SET password = $2 WHERE id = $1")
//    Mono<Void> setPassword(String id, String newPassword);

//    @Query("UPDATE wtc.users SET password = $3 WHERE id = $1 AND password = $2")
//    Mono<Void> setPassword(String id, String oldPassword, String newPassword);
}
