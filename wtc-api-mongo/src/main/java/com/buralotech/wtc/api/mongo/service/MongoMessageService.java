/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.mongo.service;

import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.mongo.domain.MessageImpl;
import com.buralotech.wtc.api.service.database.AbstractMessageService;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Clock;

@Service
public class MongoMessageService extends AbstractMessageService {

    private static final long MAX_DOCUMENTS = 5000;
    private static final long MAX_COLLECTION_SIZE = 1048576;
    private static final String MESSAGES_COLLECTION = "messages_";
    private static final String MESSAGE_TIMESTAMP_COLUMN = "messageTimestamp";
    private final ReactiveMongoOperations mongoOperations;
    private final CollectionOptions collectionOptions;

    MongoMessageService(final ReactiveMongoOperations mongoOperations,
                        final IdentifierService identifierService,
                        final Clock clock) {
        super(identifierService, clock);
        this.mongoOperations = mongoOperations;
        collectionOptions = CollectionOptions.empty().capped().maxDocuments(MAX_DOCUMENTS).size(MAX_COLLECTION_SIZE);
    }

    @Override
    public Mono<Void> createMessageStream(final String room) {
        return mongoOperations
                .createCollection(getCollectionName(room), collectionOptions)
                .then();
    }

    @Override
    public Mono<Void> deleteMessageStream(final String room) {
        return mongoOperations
                .dropCollection(getCollectionName(room))
                .then();
    }

    @Override
    public Flux<Message> fetchMessages(final String room,
                                       final String marker) {
        return mongoOperations.find(createQuery(Long.parseLong(marker)), Message.class, getCollectionName(room));
    }

//    @Override
//    public Flux<Message> streamMessages(final String room,
//                                        final String marker) {
//        return mongoOperations.tail(createQuery(Long.parseLong(marker)), Message.class, getCollectionName(room));
//    }

    private Query createQuery(final long timestamp) {
        return new Query().addCriteria(Criteria.where(MESSAGE_TIMESTAMP_COLUMN).gt(timestamp));

    }

    @Override
    public Mono<Message> postMessage(final String room,
                                     final String sender,
                                     final String language,
                                     final String text) {
        return getIdentifierService()
                .generateIdentifier()
                .map(id -> new MessageImpl(id, sender, language, text, getClock().millis()))
                .flatMap(message -> mongoOperations.insert(message, getCollectionName(room)));
    }

    private String getCollectionName(final String room) {
        return MESSAGES_COLLECTION + room.replace("-", "");
    }
}
