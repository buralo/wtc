/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.service.database;

import com.buralotech.wtc.api.domain.User;
import com.buralotech.wtc.api.domain.UserFactory;
import com.buralotech.wtc.api.service.id.IdentifierService;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import reactor.core.publisher.Mono;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

public abstract class AbstractUserService implements UserService {

    private final UserFactory userFactory;

    private final IdentifierService identifierService;

    private final JavaMailSender emailSender;

    protected AbstractUserService(final UserFactory userFactory,
                                  final IdentifierService identifierService,
                                  final JavaMailSender emailSender) {
        this.userFactory = userFactory;
        this.identifierService = identifierService;
        this.emailSender = emailSender;
    }

    protected final IdentifierService getIdentifierService() {
        return identifierService;
    }

    protected final UserFactory getUserFactory() {
        return userFactory;
    }

    protected final Mono<String> generateToken() {
        return Mono.defer(() -> Mono.just(UUID.randomUUID().toString()));
    }

    protected final Mono<User> createUserEntity(final String name,
                                                final String nickname,
                                                final String emailAddress) {
        return identifierService
                .generateIdentifier()
                .zipWith(generateToken())
                .map(tuple -> userFactory.createUser(tuple.getT1(), name, nickname, emailAddress, tuple.getT2(), null));
    }

    protected final Mono<User> sendEnrolmentEmail(final User user) {
        return Mono
                .just(user)
                .flatMap(this::doSendEnrolmentEmail);
    }

    private Mono<User> doSendEnrolmentEmail(final User user) {
        try {
            final MimeMessage message = emailSender.createMimeMessage();
            final MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(new InternetAddress(user.getEmailAddress(), user.getName()));
            helper.setSubject("[Action required] What's the craic? - Registration");
            helper.setText("In order to complete your registration you need to confirm your e-mail address by clicking on the link below:\n\n" +
                    "http://localhost:8090/activate.html?user" + user.getId() + "&token=" + user.getToken());
            emailSender.send(message);
            return Mono.just(user);
        } catch (final UnsupportedEncodingException | MessagingException e) {
            return Mono.error(e);
        }
    }
}
