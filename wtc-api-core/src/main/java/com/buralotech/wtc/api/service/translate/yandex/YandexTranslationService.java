/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.service.translate.yandex;

import com.buralotech.wtc.api.config.YandexTranslateProperties;
import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.domain.MessageFactory;
import com.buralotech.wtc.api.service.translate.TranslationService;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Collections;

@Service
@Profile("yandex")
public class YandexTranslationService implements TranslationService {

    private final MessageFactory messageFactory;

    private final WebClient webClient;

    YandexTranslationService(final MessageFactory messageFactory,
                             final YandexTranslateProperties properties) {
        this.messageFactory = messageFactory;
        this.webClient = WebClient.builder()
                .baseUrl(properties.getUrl())
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .defaultUriVariables(Collections.singletonMap("key", properties.getKey()))
                .build();
    }

    @Override
    public Mono<Message> translate(final String to,
                                   final Message message) {
        if (to.equalsIgnoreCase(message.getMessageLanguage())) {
            return Mono.just(message);
        } else {
            return webClient
                    .post()
                    .uri("/api/v1.5/tr.json/translate?lang={lang}&key={key}", Collections.singletonMap("lang", message.getMessageLanguage() + '-' + to))
                    .body(BodyInserters.fromFormData("text", message.getMessageText()))
                    .retrieve()
                    .bodyToMono(String.class)
                    .map(text -> messageFactory.createMessage(
                            message.getId(),
                            message.getSender(),
                            to,
                            text,
                            message.getMessageTimestamp()));
        }
    }
}
