package com.buralotech.wtc.api.domain;

public interface MessageFactory {

    Message createMessage(
            String id,
            String sender,
            String messageLanguage,
            String messageText,
            long messageTimestamp);
}
