/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.service.database;

import com.buralotech.wtc.api.domain.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {

    Flux<User> fetchUsers();

    Mono<User> createUser(String name, String nickname, String emailAddress);

    Mono<User> getUser(String id);

    Mono<Void> updateUser(String id, String name, String nickname, String emailAddress);

    Mono<Void> setUserPassword(String id, String token, String newPassword);

    Mono<Void> changeUserPassword(String id, String oldPassword, String newPassword);

    Mono<Void> deleteUser(String id);
}
