/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.service.database;

import com.buralotech.wtc.api.domain.Message;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MessageService {

    Mono<Void> createMessageStream(String room);

    Mono<Void> deleteMessageStream(String room);

    Flux<Message> fetchMessages(String room);

    Flux<Message> fetchMessages(String room, String marker);

    Flux<Message> streamMessages(final String room);

    Flux<Message> streamMessages(String room, String marker);

    Mono<Message> postMessage(String room, String sender, String language, String text);
}
