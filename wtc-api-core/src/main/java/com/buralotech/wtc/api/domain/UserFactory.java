package com.buralotech.wtc.api.domain;

public interface UserFactory {

    User createUser(
            String id,
            String name,
            String nickname,
            String emailAddress,
            String token,
            String password);
}
