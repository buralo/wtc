/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.service.database;

import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.service.id.IdentifierService;
import reactor.core.publisher.Flux;

import java.time.Clock;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicReference;

public abstract class AbstractMessageService implements MessageService {

    private final IdentifierService identifierService;

    private final Clock clock;

    protected AbstractMessageService(final IdentifierService identifierService,
                                     final Clock clock) {
        this.identifierService = identifierService;
        this.clock = clock;
    }

    protected final IdentifierService getIdentifierService() {
        return identifierService;
    }

    protected final Clock getClock() {
        return clock;
    }

    @Override
    public Flux<Message> fetchMessages(final String room) {
        return fetchMessages(room, getStartMarker());
    }


    @Override
    public Flux<Message> streamMessages(final String room) {
        return streamMessages(room, getStartMarker());
    }

    @Override
    public Flux<Message> streamMessages(final String room,
                                        final String marker) {
        final AtomicReference<String> last = new AtomicReference<>(marker);
        return Flux
                .interval(Duration.ZERO, Duration.ofSeconds(1))
                .zipWith(Flux.<AtomicReference<String>>generate(generator -> generator.next(last)))
                .flatMap(s ->
                        fetchMessages(room, s.getT2().get())
                                .doOnNext(message -> s.getT2().set(message.getMarker())));
    }

    protected String getStartMarker() {
        return "-1";
    }
}
