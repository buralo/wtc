/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.service.translate.google;

import com.buralotech.wtc.api.domain.Message;
import com.buralotech.wtc.api.domain.MessageFactory;
import com.buralotech.wtc.api.service.translate.TranslationService;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateException;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Profile("google")
public class GoogleTranslationService implements TranslationService {

    private final MessageFactory messageFactory;

    private final Translate translate;

    GoogleTranslationService(final MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
        this.translate = TranslateOptions.getDefaultInstance().getService();
    }

    @Override
    public Mono<Message> translate(final String to,
                                   final Message message) {

        if (to.equalsIgnoreCase(message.getMessageLanguage())) {
            return Mono.just(message);
        } else {
            try {
                final Translation translation =
                        translate.translate(
                                message.getMessageText(),
                                Translate.TranslateOption.sourceLanguage(message.getMessageLanguage()),
                                Translate.TranslateOption.targetLanguage(to));

                return Mono.just(
                        messageFactory.createMessage(
                                message.getId(),
                                message.getSender(),
                                to,
                                translation.getTranslatedText(),
                                message.getMessageTimestamp()));
            } catch (final TranslateException e) {
                return Mono.error(e);
            }
        }
    }
}
