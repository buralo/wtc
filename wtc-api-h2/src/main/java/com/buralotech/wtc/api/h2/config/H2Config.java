/*
 * Copyright 2019-2024 Búraló Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.buralotech.wtc.api.h2.config;

import io.r2dbc.h2.H2ConnectionConfiguration;
import io.r2dbc.h2.H2ConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.core.DatabaseClient;

@Configuration
@EnableConfigurationProperties(H2Properties.class)
public class H2Config extends AbstractR2dbcConfiguration {

    private final H2Properties properties;

    H2Config(final H2Properties properties) {
        this.properties = properties;
    }

    @Override
    public ConnectionFactory connectionFactory() {
        return new H2ConnectionFactory(H2ConnectionConfiguration
                .builder()
                .inMemory(properties.getDatabase())
                .username(properties.getUsername())
                .password(properties.getPassword())
                .build());
    }

    @EventListener(ApplicationReadyEvent.class)
    public void createSchema(ApplicationReadyEvent event) {
        final DatabaseClient client = event.getApplicationContext().getBean(DatabaseClient.class);
        event.getApplicationContext().getResource("");
        client.execute("CREATE SCHEMA IF NOT EXISTS wtc;").fetch().rowsUpdated().subscribe();
    }
}